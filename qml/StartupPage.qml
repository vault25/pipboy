import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import Pipboy 1.0 as Pipboy

Item {
    id: base
    property bool running: false
    property bool completed: false

    property QtObject soundEffects

    function start() {
        if(!running) {
            completed = false
            d.currentLine = 0
            d.currentStep = -1
            d.currentDelay = 0
            d.completeText = ""
            running = true
        }
    }

    property var texts: [
        { left: "Starting RobCo Industries Pip-OS(R)", right: "", delay: 0 },
        { left: "Please Stand By...", right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "Initializing hardware... ", right: "[OK]", delay: 20 },
        { left: "64k RAM System", right: "", delay: 0 },
        { left: "38911 Bytes Free", right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "Loading kernel... ", right: "[OK]", delay: 20 },
        { left: "Verifying kernel integrity... ", right: "[FAIL]", delay: 40 },
        { left: "ERROR! Could not verify kernel integrity! Do you wish to continue? [Y/N]:", right: " Error 0x00B636C6", delay: 40 },
        { left: "Starting kernel... ", right: "[OK]", delay: 20 },
        { left: "", right: "", delay: 0 },
        { left: "Pip-OS(R) v%1".arg(Pipboy.Constants.version), right: "", delay: 0 },
        { left: "Copyright (c) %1 RobCo Industries".arg(Pipboy.Constants.buildYear), right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "Starting input services... ", right: "[WARN]", delay: 20 },
        { left: "WARNING: Keyboard not found!", right: "", delay: 0 },
        { left: "Starting storage services... ", right: "[OK]", delay: 20 },
        { left: "No holotape found.", right: "", delay: 0 },
        { left: "Starting communication services... ", right: "[OK]", delay: 40 },
        { left: "Checking for updates... ", right: "[FAIL]", delay: 80 },
        { left: "Could not contact primary update servers!", right: "", delay: 0 },
        { left: "Contacting secondary update servers... ", right: "[FAIL]", delay: 80 },
        { left: "Could not contact secondary update servers!", right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "WARNING! This version of PipOS is %1 years old.".arg(d.pipOSAge), right: "", delay: 0 },
        { left: "Automatic download of the most recent version of PipOS failed. Please contact the nearest RobCo Industries representative as soon as possible to obtain the latest version of PipOS.", right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "Starting graphical display service... ", right: "[OK]", delay: 40 },
        { left: "Load ROM: DEITRIX 286", right: "", delay: 20 }
    ]

    ScrollView {
        id: textScrollView
        anchors.fill: parent
        anchors.margins: 20
        focus: true

        TextArea {
            id: startupText
            text: d.completeText

            cursorPosition: d.completeText.length
            cursorVisible: true

            readOnly: true

            focus: true
        }
    }

    Timer {
        running: base.running
        interval: 17
        repeat: true
        onTriggered: {
            if(d.currentDelay > 0) {
                d.currentDelay -= 1;
                return
            }

            d.currentStep += 1
            base.soundEffects.play("text_scroll")
            if(d.currentStep >= d.completeLine.length) {
                if(d.currentLine + 1 >= base.texts.length) {
                    base.running = false
                    base.completed = true
                    return
                }

                d.currentLine += 1
                d.currentStep = -1
                d.completeText += "\n"
            } else {
                d.completeText += d.completeLine[d.currentStep]
                if(d.currentText.delay > 0 && d.currentStep === d.pausePosition) {
                    d.currentDelay = d.currentText.delay
                }
            }
        }
    }

    FontMetrics {
        id: metrics
        font: startupText.font
    }

    QtObject {
        id: d

        property int currentLine: 0
        property int currentStep: -1
        property int currentDelay: 0

        property real lineWidth: Math.floor((textScrollView.width - 40) / metrics.averageCharacterWidth)

        property var currentText: base.texts[currentLine]
        property string completeLine: currentText.left + currentText.right

        property int pausePosition: currentText.left.length - 1

        property string completeText: ""

        property string pipOSAge: Qt.formatDate(Pipboy.Constants.inGameDate, "yyyy") - Pipboy.Constants.buildYear;
    }
}
