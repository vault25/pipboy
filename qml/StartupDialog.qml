import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Zax.Identity 1.0 as Identity

import Pipboy 1.0 as Pipboy

Dialog {
    id: dialog

    x: parent.width / 2 - width / 2
    y: parent.height / 2 - height / 2
    width: 400
    height: 300

    title: stack.currentItem.title

    closePolicy: Dialog.NoAutoClose

    onOpened: ApplicationWindow.window.states[0].duration = -1

    property bool error: false

    StackView {
        id: stack

        anchors.fill: parent

        initialItem: openingPage
    }

    Component {
        id: openingPage

        Page {
            title: "Welcome"

            ColumnLayout {
                anchors.fill: parent

                Label {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    wrapMode: Text.Wrap
                    font: Pipboy.Style.fonts.small
                    text: "This is the Pipboy application for the Vault 25 LARP. "
                        + "To get started, some initial setup is needed. "
                        + "Please press 'Next' to continue."
                }

                Label {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    wrapMode: Text.Wrap
                    font: Pipboy.Style.fonts.small
                    text: "Note: Dialogs like these with a white border are considered "
                        + "out of game and any information in them is not something "
                        + "your character knows."
                }
            }

            footer: DialogButtonBox {
                standardButtons: DialogButtonBox.Ok

                onAccepted: stack.push(characterPage)

                Component.onCompleted: standardButton(DialogButtonBox.Ok).text = "Next"
            }
        }
    }

    Component {
        id: characterPage

        Page {
            title: "Character Name"

            ColumnLayout {
                anchors.fill: parent

                Label {
                    Layout.fillWidth: true
                    text: "Please enter your character name:"
                    wrapMode: Text.Wrap
                }

                TextField {
                    id: nameField
                    Layout.fillWidth: true
                    onTextChanged: dialog.error = false
                }

                Label {
                    Layout.fillWidth: true
                    visible: (!nameField.text || identityBuilder.available) && !dialog.error
                    wrapMode: Text.Wrap
                    font: Pipboy.Style.fonts.small
                    text: "Your character name is used to identify you in the "
                        + "system and will give you access to several features. "
                        + "It also allows game masters to give you access to "
                        + "certain extra functionality."
                }

                Label {
                    id: errorLabel
                    Layout.fillWidth: true
                    visible: (nameField.text && !identityBuilder.available) || dialog.error
                    wrapMode: Text.Wrap
                    font: Pipboy.Style.fonts.small
                    text: "Someone is already using that name. If you think this "
                        + "is an error, please contact a game master."
                }

                Item { Layout.fillHeight: true }

                Identity.Builder {
                    id: identityBuilder

                    properties: {
                        return { "field_character": nameField.text }
                    }
                }

                Connections {
                    target: Identity.Self

                    function onChanged() {
                        if (!Identity.Self.valid) {
                            return
                        }

                        Pipboy.Preferences.general.userId = Identity.Self.id
                        stack.push(permissionsPage)
                    }

                    function onError(errorCode, errorString) {
                        dialog.error = true
                        errorLabel.text = "Error " + errorCode + ": " + errorString
                        buttonBox.standardButton(DialogButtonBox.Ok).text = "Skip"
                    }
                }
            }

            footer: DialogButtonBox {
                id: buttonBox

                standardButtons: DialogButtonBox.Ok

                enabled: identityBuilder.available || dialog.error

                Component.onCompleted: standardButton(DialogButtonBox.Ok).text = "Next"

                onAccepted: identityBuilder.create()
            }
        }
    }

    Component {
        id: permissionsPage

        Page {
            title: "Permissions"

            Label {
                anchors.fill: parent

                wrapMode: Text.Wrap
                font: Pipboy.Style.fonts.small
                text: "Some features require special permissions. When you press "
                    + "next, the Location and Storage permissions will be requested. "
                    + "Your location is used to calculate how close you are to "
                    + "things like radiation sources. Storage is used to store "
                    + "any notes you write."
            }

            footer: DialogButtonBox {
                Button {
                    text: "Next"
                    DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
                }
                //standardButtons: DialogButtonBox.Ok

                //Component.onCompleted: standardButton(DialogButtonBox.Ok).text = "Next"

                onAccepted: permissionManager.request()
            }

            Pipboy.PermissionManager {
                id: permissionManager

                onGrantedChanged: {
                    if (granted) {
                        stack.push(finishedPage)
                    } else {
                        stack.push(errorPage)
                    }
                }
            }
        }
    }

    Component {
        id: errorPage

        Page {
            title: "Error"

            Label {
                anchors.fill: parent

                wrapMode: Text.Wrap
                font: Pipboy.Style.fonts.small

                text: "Requesting permissions failed. Some features will not be "
                    + "available. Please press 'Back' to try again or 'Next' to "
                    + "continue anyway."
            }

            footer: DialogButtonBox {
                id: buttonBox
                Button {
                    text: "Back"
                    onClicked: stack.pop()
                }

                Button {
                    text: "Next"
                    onClicked: stack.push(finishedPage)
                }
            }
        }
    }

    Component {
        id: finishedPage

        Page {
            title: "Finished"

            Label {
                anchors.fill: parent

                wrapMode: Text.Wrap
                font: Pipboy.Style.font.small
                text: "The initial setup is now completed and you should be ready "
                    + "to go. Have a great event!"
            }

            footer: DialogButtonBox {
                standardButtons: DialogButtonBox.Close

                onRejected: dialog.accept()
            }
        }
    }
}
