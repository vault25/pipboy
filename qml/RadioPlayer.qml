import QtQuick 2.15
import QtMultimedia 5.15

pragma Singleton

Item {
    property alias source: player.source

    property bool playing: player.playbackState == MediaPlayer.PlayingState

    property alias mediaPlayer: player

    function play() {
        player.play()
    }

    function stop() {
        player.stop()
    }

    MediaPlayer {
        id: player;
        loops: MediaPlayer.Infinite
        audioRole: MediaPlayer.MusicRole
    }
}
