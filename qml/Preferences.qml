import QtQuick 2.7

import Qt.labs.settings 1.0

pragma Singleton

Item {
    id: root

    property Settings general: Settings {
        property int lowPowerTimeout: 1000 * 60 * 2
        property bool displayAlwaysOn: true
        property date lastDisplayedMessage;
        property string userId;

        property int totalRadiation: 0

        property bool trackerEnabled: true
    }

    property Settings windowMargins: Settings {
        category: "window"

        property int left: 0
        property int right: 0
        property int top: 0
        property int bottom: 0
    }

    property QtObject development: QtObject {
        property bool showLocation: false
    }
}
