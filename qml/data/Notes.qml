import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Pipboy 1.0 as Pipboy

import "../controls"

ListWithDetails {
    id: list

    function showEditPage() {
        if (Pipboy.Breakdowns.activeEffect == "no_input" || Pipboy.Breakdowns.activeEffect == "no_editing") {
            return
        }

        let noteText = list.model.contents(list.currentIndex)

        let page = ApplicationWindow.window.stack.push(Qt.resolvedUrl("../pages/NoteEditPage.qml"), {
            title: list.currentItem.text,
            text: noteText
        })
        page.close.connect(function() {
            ApplicationWindow.window.stack.pop()
            if (noteText != page.text) {
                if (list.model.saveContents(list.currentIndex, page.text)) {
                    list.currentIndexChanged()
                    ApplicationWindow.window.showPopupMessage("Saved %1".arg(list.currentItem.text))
                }
            }
        })
    }

    model: Pipboy.NotesModel { }

    delegate: FocusScope {
        width: ListView.view.width
        height: item.implicitHeight

        property alias text: item.text
        property bool edit: false;

        ItemDelegate {
            id: item
            anchors.fill: parent
            highlighted: parent.ListView.isCurrentItem
            visible: !parent.edit
            text: model.fileName

            onClicked: parent.ListView.view.currentIndex = index
        }

        TextField {
            id: nameField
            anchors.fill: parent
            text: model.fileName

            visible: parent.edit
            focus: parent.edit
            enabled: Pipboy.Breakdowns.activeEffect != "no_editing"

            onAccepted: {
                base.model.renameNote(index, text)
                parent.edit = false
            }
            onActiveFocusChanged: {
                if(!activeFocus && parent.edit) parent.edit = false;
            }
            Keys.onEscapePressed: parent.edit = false;
        }
    }

    footer: ItemDelegate {
        width: ListView.view.width
        text: "Add note...";
        onClicked: list.model.createNote();
        enabled: Pipboy.Breakdowns.activeEffect != "no_editing"
    }

    details: ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.fillWidth: true
            Button {
                Layout.fillWidth: true
                text: "Edit";
                flat: true;
                enabled: Pipboy.Breakdowns.activeEffect != "no_editing"
                onClicked: list.showEditPage()
            }
            Button {
                id: renameButton;
                Layout.fillWidth: true;
                text: "Ren";
                flat: true;
                enabled: Pipboy.Breakdowns.activeEffect != "no_editing"
                onClicked: {
                    list.currentItem.edit = true;
                    list.currentItem.forceActiveFocus()
                }
            }
            Button {
                id: deleteButton
                Layout.fillWidth: true;
                text: "Del";
                flat: true;
                enabled: Pipboy.Breakdowns.activeEffect != "no_editing"
                onClicked: {
                    list.model.deleteNote(list.currentIndex);
                }
            }
        }

        Flickable {
            id: noteContentsView
            Layout.fillHeight: true
            Layout.fillWidth: true

            contentWidth: width
            contentHeight: noteContents.implicitHeight

            ScrollIndicator.vertical: ScrollIndicator {
                anchors.right: parent.right;
                anchors.rightMargin: Pipboy.Style.margins.large
            }

            boundsBehavior: Flickable.StopAtBounds
            clip: true

            Label {
                id: noteContents

                width: noteContentsView.width

                text: list.currentIndex >= 0 ? list.model.contents(list.currentIndex) : ""
                wrapMode: TextEdit.Wrap
            }

            TapHandler {
                onTapped: list.showEditPage()
            }
        }
    }
}
