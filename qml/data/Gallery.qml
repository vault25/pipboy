import QtQuick 2.7
import QtQuick.Controls 2.2

import Pipboy 1.0 as Pipboy

import "../controls" as Controls

GridView
{
    id: grid

    model: Pipboy.GalleryModel { }
    cellWidth: parent.width / 4
    cellHeight: cellWidth
    boundsBehavior: Flickable.StopAtBounds

    clip: true

    cacheBuffer: 0

    ScrollIndicator.vertical: ScrollIndicator {
        anchors.right: parent.right
        anchors.rightMargin: Pipboy.Style.margins.large
    }

    delegate: Controls.GridDelegate {
        width: GridView.view.cellWidth
        height: GridView.view.cellHeight

        Image {
            anchors.fill: parent
            anchors.margins: 5
            source: model.filePath
            fillMode: Image.PreserveAspectFit;
            sourceSize.width: parent.width
            sourceSize.height: parent.height
            cache: false
            asynchronous: true
        }

        onClicked: {
            let page = ApplicationWindow.window.stack.push(Qt.resolvedUrl("../pages/ImagePage.qml"), { source: model.filePath })
            print(page)
            page.close.connect(ApplicationWindow.window.stack.pop)
        }
    }

    Label {
        anchors.centerIn: parent
        visible: grid.count == 0
        horizontalAlignment: Qt.AlignHCenter
        displayText: "No files found.\nPlace them under\n" + grid.model.storagePath
    }
}
