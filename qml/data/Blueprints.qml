import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Zax.JsonApi 1.0 as JsonApi
import Zax.Identity 1.0 as Identity

import Pipboy 1.0 as Pipboy

import "../controls" as Controls

GridView
{
    id: grid

    model: JsonApi.ApiModel {
        id: blueprintModel

        path: "/node/blueprint"

        fields: {
            "body": "string"
        }

        JsonApi.FilterRule { field: "status"; value: 1 }
        JsonApi.SortRule { fields: ["field_weight", "title"] }
        JsonApi.ClientFilterRule { field: "field_devices"; value: Identity.Self.id; empty: JsonApi.IncludeRule.RejectEmpty }
    }
    cellWidth: parent.width / 4
    cellHeight: cellWidth
    boundsBehavior: Flickable.StopAtBounds

    clip: true

    cacheBuffer: 0

    ScrollIndicator.vertical: ScrollIndicator {
        anchors.right: parent.right
        anchors.rightMargin: Pipboy.Style.margins.large
    }

    function closeImage() {
        ApplicationWindow.window.stack.pop()
        ApplicationWindow.window.fullscreen = false
    }

    delegate: Controls.GridDelegate {
        width: GridView.view.cellWidth
        height: GridView.view.cellHeight

        Image {
            id: image

            anchors.fill: parent
            anchors.margins: Pipboy.Style.margins.small
            source: "image://jsonapi/node/map/%1?field=field_image".arg(model.id)
            fillMode: Image.PreserveAspectFit;
            sourceSize.width: parent.width
            sourceSize.height: parent.height
            cache: false
            asynchronous: true
        }

        Label {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top

            displayText: model.title

            leftPadding: Pipboy.Style.margins.medium
            rightPadding: Pipboy.Style.margins.medium
            topPadding: Pipboy.Style.margins.small
            bottomPadding: Pipboy.Style.margins.small
            leftInset: Pipboy.Style.sizes.borderWidth
            rightInset: Pipboy.Style.sizes.borderWidth
            topInset: Pipboy.Style.sizes.borderWidth

            background: Rectangle {
                color: "black"
                opacity: 0.5
            }
        }

        onClicked: {
            let page = ApplicationWindow.window.stack.push(Qt.resolvedUrl("../pages/ImagePage.qml"), {
                source: image.source,
                title: model.title,
                description: model.fields.body
            })
            page.close.connect(ApplicationWindow.window.stack.pop)
        }
    }

    Label {
        anchors.centerIn: parent
        visible: !blueprintModel.query.running && blueprintModel.count == 0
        horizontalAlignment: Qt.AlignHCenter
        displayText: "No blueprints found."
    }
}
