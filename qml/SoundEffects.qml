import QtQuick 2.9
import QtMultimedia 5.9

pragma Singleton;

Item {
    function play(name, volume) {
        var items = sounds[name]
        var index = Math.floor(Math.random() * items.length)

        var effect = effectBuffer.shift()
        effect.source = items[index]
        effect.volume = volume !== undefined ? volume : 1.0
        effect.play()
        effectBuffer[effectBuffer.length] = effect
    }

    property var sounds: {
        "boot_sequence": [
            "qrc:/sounds/UI_PipBoy_BootSequence_B.wav"
        ],
        "list_item": [
            "qrc:/sounds/UI_PipBoy_RotaryVertical_01.wav",
            "qrc:/sounds/UI_PipBoy_RotaryVertical_03.wav"
        ],
        "tab": [
            "qrc:/sounds/UI_PipBoy_RotaryHorizontal_01.wav",
            "qrc:/sounds/UI_PipBoy_RotaryHorizontal_02.wav"
        ],
        "radio_on": [
            "qrc:/sounds/UI_PipBoy_Radio_On.wav"
        ],
        "radio_off": [
            "qrc:/sounds/UI_PipBoy_Radio_Off.wav"
        ],
        "text_scroll": [
            "qrc:/sounds/UI_PipBoy_Favorite_Menu_Down_01.wav"
        ],
        "map_select": [
            "qrc:/sounds/UI_PipBoy_Map_Rollover_01.wav"
        ],
        "message": [
            "qrc:/sounds/message.wav"
        ],
        "tracker": [
            "qrc:/sounds/tracker.wav"
        ]
    }

    property var effectBuffer: [
        effect0, effect1, effect2, effect3, effect4, effect5, effect6, effect7, effect8, effect9
    ]

    SoundEffect { id: effect0; onPlayingChanged: source = (playing ? source : "") }
    SoundEffect { id: effect1; onPlayingChanged: source = (playing ? source : "") }
    SoundEffect { id: effect2; onPlayingChanged: source = (playing ? source : "") }
    SoundEffect { id: effect3; onPlayingChanged: source = (playing ? source : "") }
    SoundEffect { id: effect4; onPlayingChanged: source = (playing ? source : "") }
    SoundEffect { id: effect5; onPlayingChanged: source = (playing ? source : "") }
    SoundEffect { id: effect6; onPlayingChanged: source = (playing ? source : "") }
    SoundEffect { id: effect7; onPlayingChanged: source = (playing ? source : "") }
    SoundEffect { id: effect8; onPlayingChanged: source = (playing ? source : "") }
    SoundEffect { id: effect9; onPlayingChanged: source = (playing ? source : "") }
}
