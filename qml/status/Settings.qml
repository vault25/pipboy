import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.2

import Zax.JsonApi 1.0 as JsonApi
import Zax.Identity 1.0 as Identity

import Pipboy 1.0 as Pipboy
import Pipboy.GeigerCounter 1.0 as GeigerCounter

Item {
    id: settingsItem

    ColumnLayout {
        anchors.centerIn: parent

        spacing: Pipboy.Style.margins.medium

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: (Pipboy.LocationTracker.enabled ? "Disable" : "Enable") + " Object Tracker"
            checkable: true
            checked: Pipboy.LocationTracker.enabled
            visible: Pipboy.LocationTracker.locationCount > 0
            onToggled: Pipboy.Preferences.general.trackerEnabled = checked
        }

        Label {
            Layout.alignment: Qt.AlignHCenter
            text: "Low-power Timeout"
        }

        SpinBox {
            Layout.alignment: Qt.AlignHCenter

            from: 0
            to: 1000000

            value: Pipboy.Preferences.general.lowPowerTimeout
            onValueModified: Pipboy.Preferences.general.lowPowerTimeout = value

            stepSize: 1000

            textFromValue: function(text, locale) {
                var secs = Math.floor(value / 1000)
                var min = Math.floor(secs / 60)

                return  "%1m %2s".arg(min).arg(secs - (min * 60))
            }

            valueFromText: function(text, locale) {
                var min = parseInt(text)
                var secs = 0
                return (min * 60 + secs) * 1000
            }
        }

        Button {
            text: "Change Out-of-Game Settings"

            onClicked: settingsDialog.open()
        }

        Item { Layout.fillWidth: true; Layout.fillHeight: true;}
    }

    Dialog {
        id: settingsDialog

        title: "Out of Game Settings"

        x: parent.width / 2 - width / 2
        y: 0

        width: parent.width * 0.75

        height: Math.min(parent.height, contents.height * 2)

        standardButtons: Dialog.Close

        padding: Pipboy.Style.margins.small

        Flickable {
            id: flickable

            anchors.fill: parent

            boundsBehavior: Flickable.StopAtBounds
            clip: true
            contentHeight: contents.height

            GridLayout {
                id: contents

                width: parent.width

                columns: 2

                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 0
                    text: "Change Margins"

                    onClicked: marginsDialog.open()
                }

                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 0
                    text: "Clear Caches"
                    onClicked: JsonApi.Api.cache.clear()
                }

                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 0
                    text: "Reset Dosimeter"
                    onClicked: {
                        GeigerCounter.Counter.setTotalRadiation(0)
                    }
                }

                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 0
                    text: "Development Options"

                    onClicked: {
                        developmentDialog.open()
                    }
                }

                Label {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 0
                    text: "Connected as: " + Identity.Self.name
                }

                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 0
                    text: "Reset"

                    onClicked: {
                        //Identity.Self.reset()
                        settingsDialog.close()
                        ApplicationWindow.window.reboot()
                    }
                }
            }
        }
    }

    Dialog {
        id: marginsDialog

        x: parent.width / 2 - width / 2
        y: 0

        width: parent.width * 0.9
        height: parent.height * 0.9

        title: "Margins"

        standardButtons: Dialog.Close | Dialog.Reset

        onReset: {
            Pipboy.Preferences.windowMargins.top = 0
            Pipboy.Preferences.windowMargins.bottom = 0
            Pipboy.Preferences.windowMargins.left = 0
            Pipboy.Preferences.windowMargins.right = 0
        }

        Item {
            anchors.fill: parent

            SpinBox {
                anchors.top: parent.top;
                anchors.horizontalCenter: parent.horizontalCenter

                value: Pipboy.Preferences.windowMargins.top
                onValueModified: Pipboy.Preferences.windowMargins.top = value

                from: 0
                to: 1000
            }

            SpinBox {
                anchors.bottom: parent.bottom;
                anchors.horizontalCenter: parent.horizontalCenter

                value: Pipboy.Preferences.windowMargins.bottom
                onValueModified: Pipboy.Preferences.windowMargins.bottom = value

                from: 0
                to: 1000
            }

            SpinBox {
                anchors.left: parent.left;
                anchors.verticalCenter: parent.verticalCenter

                value: Pipboy.Preferences.windowMargins.left
                onValueModified: Pipboy.Preferences.windowMargins.left = value

                from: 0
                to: 1000
            }

            SpinBox {
                anchors.right: parent.right;
                anchors.verticalCenter: parent.verticalCenter

                value: Pipboy.Preferences.windowMargins.right
                onValueModified: Pipboy.Preferences.windowMargins.right = value

                from: 0
                to: 1000
            }
        }
    }

    Dialog {
        id: developmentDialog

        x: parent.width / 2 - width / 2
        y: 0

        width: parent.width * 0.9
        height: parent.height * 0.9

        title: "Development"

        standardButtons: Dialog.Close

        Flickable {
            anchors.fill: parent
            clip: true

            ColumnLayout {
                width: parent.width

                Button {
                    text: "Trigger Breakdown"
                    onClicked: Pipboy.Breakdowns.trigger()
                }

                Label {
                    visible: Pipboy.Breakdowns.activeBreakdown

                    wrapMode: Text.Wrap

                    text: "Breakdown Effect: %1 ".arg(Pipboy.Breakdowns.activeEffect)
                }

                Label {
                    Layout.fillWidth: true
                    text: GeigerCounter.Counter.locationInformation
                }

                Label {
                    Layout.fillWidth: true

                    text: "Tracker Locations: " + Pipboy.LocationTracker.locationCount
                }
            }
        }
    }
}
