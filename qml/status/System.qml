import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import Pipboy 1.0 as Pipboy
import Pipboy.GeigerCounter 1.0 as GeigerCounter

import "../controls"

Item {
    ListView {
        anchors.fill: parent
        anchors.leftMargin: Pipboy.Style.margins.large * 4
        anchors.rightMargin: Pipboy.Style.margins.large * 4
        boundsBehavior: ListView.StopAtBounds

        spacing: Pipboy.Style.margins.medium

        clip: true

        model: Pipboy.SystemStatusModel {
            handler: Pipboy.Breakdowns
            geigerCounter: GeigerCounter.Counter
        }

        delegate: MessageDelegate {
            width: ListView.view.width

            contentItem: Item {
                implicitHeight: subsystem.implicitHeight + (message.visible ? message.implicitHeight + Pipboy.Style.margins.medium : 0)

                Label {
                    id: subsystem

                    anchors.left: parent.left
                    displayText: model.subsystem
                }

                Label {
                    anchors.right: parent.right
                    displayText: switch(model.status) {
                        case Pipboy.SystemStatusModel.Ok: return "OK"
                        case Pipboy.SystemStatusModel.Warning: return "WARN"
                        default: return "ERR"
                    }
                }

                Label {
                    id: message

                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom

                    visible: text != ""
                    wrapMode: Text.Wrap

                    displayText: model.reason != "" ? "Error %1\n%2".arg(model.code).arg(model.reason) : ""
                }
            }
        }

        footer: MessageDelegate {
            width: ListView.view.width
            height: rebootButton.height + Pipboy.Style.margins.large * 2

            topInset: Pipboy.Style.margins.medium
            bottomInset: Pipboy.Style.margins.medium

            contentItem: RowLayout {
                Item { Layout.fillWidth: true }

                Button {
                    id: rebootButton
                    flat: true;
                    text: "Reboot"
                    onClicked: reboot()
                }

                Button {
                    flat: true
                    text: "Factory Reset"
                    onClicked: factoryReset()
                }

                Item { Layout.fillWidth: true }
            }
        }
    }
}
