import QtQuick 2.9
import QtQuick.Controls 2.2

import Pipboy 1.0 as Pipboy

Item {
    id: timeDatePanel

    property date now: Pipboy.Constants.inGameDate;
    property string dayName;

    Column {
        anchors.centerIn: parent
        Image {
            width: parent.width
            source: "qrc:/images/vault-tec-logo.png"
            fillMode: Image.PreserveAspectFit
        }
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            displayText: Qt.formatTime(new Date(), "hh:mm");
            font.pointSize: 50
        }
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            displayText: Qt.formatDate(new Date(), "dddd")
        }
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            displayText: Qt.formatDate(Pipboy.Constants.inGameDate, "d MMMM")
        }
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            displayText: Qt.formatDate(Pipboy.Constants.inGameDate, "yyyy")
        }
    }
}
