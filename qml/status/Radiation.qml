import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.2

import Pipboy 1.0 as Pipboy
import Pipboy.GeigerCounter 1.0 as GeigerCounter

Item {

    Dial {
        id: dial

        anchors.right: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        width: height
        height: Math.min(parent.width / 3, parent.height * 0.8)

        from: 0
        to: 1000

        value: GeigerCounter.Counter.currentRadiation
        Behavior on value { SpringAnimation { spring: 1.5; damping: 0.2 } }

        enabled: false

        Label {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: Pipboy.Style.margins.large
            anchors.horizontalCenter: parent.horizontalCenter
            displayText: "Röntgen"
        }
    }

    ColumnLayout {
        anchors.left: parent.horizontalCenter
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: dial.width

        Label {
            Layout.fillWidth: true
            text: "Absorbed Dose:\n" + Math.round(GeigerCounter.Counter.totalRadiation / 100) + " rad"
            wrapMode: Text.Wrap
        }

        Image {
            Layout.fillWidth: true
            Layout.preferredHeight: dial.height * 0.5
            Layout.alignment: Qt.AlignHCenter
            source: "qrc:/images/radiation-warning.png"
            fillMode: Image.PreserveAspectFit
            visible: dial.value > 50

            SequentialAnimation on opacity {
                loops: Animation.Infinite;
                PauseAnimation { duration: 1000 }
                NumberAnimation { from: 0.25; to: 1; duration: 10}
                PauseAnimation { duration: 1000 }
                NumberAnimation { from: 1; to: 0.25; duration: 10}
            }

            Label {
                anchors.top: parent.bottom
                anchors.topMargin: Pipboy.Style.margins.medium
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width

                displayText: "DANGER RADIATION HAZARD";
                font: Pipboy.Style.fonts.capsBold
                visible: dial.value > 50
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
            }
        }

        Item { Layout.fillWidth: true; Layout.fillHeight: true }
    }

    onVisibleChanged: {
        if(!ApplicationWindow.window)
            return;

        if(visible) {
            ApplicationWindow.window.radiationWarning.active = false
        } else {
            ApplicationWindow.window.radiationWarning.active = true
        }
    }
}
