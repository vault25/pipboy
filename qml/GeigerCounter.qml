import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import QtMultimedia 5.15

import Pipboy 1.0 as Pipboy

pragma Singleton;

Pipboy.GeigerCounter {
    id: counter

    property bool soundEnabled: true

    onCurrentRadiationChanged: {
        if(!soundEnabled) {
            return;
        }

        let currentSound = ""

        if(currentRadiation > 860) {
            currentSound = "radiation_g"
        } else if(currentRadiation > 725) {
            currentSound = "radiation_f"
        } else if(currentRadiation > 590) {
            currentSound = "radiation_e"
        } else if(currentRadiation > 455) {
            currentSound = "radiation_d"
        } else if(currentRadiation > 320) {
            currentSound = "radiation_c"
        } else if(currentRadiation > 185) {
            currentSound = "radiation_b"
        } else if(currentRadiation > 50) {
            currentSound = "radiation_a"
        } else {
            counter.soundEffect.stop()
            return
        }

        let entries = counter.sounds[currentSound]
        let index = Math.floor(Math.random() * entries.length)
        counter.soundEffect.source = entries[index]
        counter.soundEffect.play()
    }

    onTotalRadiationChanged: {
        Pipboy.Preferences.general.totalRadiation = totalRadiation
    }

    Component.onCompleted: {
        setTotalRadiation(Pipboy.Preferences.general.totalRadiation)
    }

    property SoundEffect soundEffect: SoundEffect {
        loops: SoundEffect.Infinite
    }

    property var sounds: {
        "radiation_a": [
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_A_01.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_A_02.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_A_03.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_A_04.wav"
        ],
        "radiation_b": [
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_B_01.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_B_02.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_B_03.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_B_04.wav"
        ],
        "radiation_c": [
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_C_01.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_C_02.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_C_03.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_C_04.wav"
        ],
        "radiation_d": [
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_D_01.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_D_02.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_D_03.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_D_04.wav"
        ],
        "radiation_e": [
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_E_01.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_E_02.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_E_03.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_E_04.wav"
        ],
        "radiation_f": [
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_F_01.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_F_02.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_F_03.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_F_04.wav"
        ],
        "radiation_g": [
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_G_01.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_G_02.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_G_03.wav",
            "qrc:/sounds/radiation/UI_PipBoy_Radiation_G_04.wav"
        ]
    }
}

