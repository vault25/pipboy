import QtQuick 2.7

pragma Singleton

QtObject {
    id: style

    property QtObject colors: QtObject {
        property color primary: __advancedGL ? "#ffffff" : "#1df21c"
        property color secondary: __advancedGL ? "#aaaaaa" : Qt.darker("#1df21c", 1.5)
        property color tertiary: __advancedGL ? "#666666" : Qt.darker("#1df21c", 2.0)
        property color primaryText: "black"

        property color background: "black"
        property color alternativeBackground: "#073d07"

        property color monochrome: "#1df21c"

        property color ocPrimary: "white"
        property color ocBackground: "black"
    }

    property QtObject sizes: QtObject {
        property real borderWidth: 2.0
        property real cornerRadius: 2.0
    }

    property QtObject margins: QtObject {
        property real small: 5.0
        property real medium: 10.0
        property real large: 20.0
    }

    property QtObject animations: QtObject {
        property int shorter: 75
        property int medium: 150
        property int longer: 300
    }

    property QtObject fonts: QtObject {
        property font small: Qt.font({famility: "Monofonto", pointSize: 12})
        property font normal: Qt.font({family: "Monofonto", pointSize: 16})
        property font normalUpperCase: Qt.font({family: "Monofonto", pointSize: 16, capitalization: 1})
        property font normalBold: Qt.font({family: "Monofonto", pointSize: 16, bold: true})
        property font caps: Qt.font({family: "Monofonto", pointSize: 16, capitalization: 1})
        property font capsBold: Qt.font({family: "Monofonto", pointSize: 16, bold: true, capitalization: 1})
    }

    property var plainDisplayText: function(input) { return input }
    property var corruptedDisplayText: function(input) { return input.replace(replacePattern, replaceWith) }

    property var displayText: plainDisplayText

    property var replacePattern: /[aeiouy]/ig
    property var replaceWith: function(match) {
        return replaceWithCharacters[parseInt(Math.random() * replaceWithCharacters.length)]
    }

    property bool lowPower: false

    property string activeEffect
    onActiveEffectChanged: {
        if (activeEffect == "text_corruption") {
            replacePattern = replacePatterns[parseInt(Math.random() * replacePatterns.length)]
            timer.interval = 750 + Math.random() * 1250
            displayText = corruptedDisplayText
        } else {
            displayText = plainDisplayText
        }
    }

    property var timer: Timer {
        running: style.activeEffect == "text_corruption" && !style.lowPower
        interval: 1000
        repeat: true
        onTriggered: {
            replacePattern = replacePatterns[parseInt(Math.random() * replacePatterns.length)]
            displayTextChanged()
        }
    }

    property var replacePatterns: [
        /[aeiouy]/ig,
        /[qwerty]/ig,
        /[asdfgh]/ig,
        /[uiop12]/ig,
        /[jkl345]/ig,
        /[zxcvbn]/ig,
        /[nm6789]/ig
    ]

    property string replaceWithCharacters:
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        + "`~!@#$%^&*()_+-=[{}];:'\",<.>/?`~!@#$%^&*()_+-=[{}];:'\",<.>/?"
        + "`~!@#$%^&*()_+-=[{}];:'\",<.>/?`~!@#$%^&*()_+-=[{}];:'\",<.>/?"
        + "ĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁł"
        + "ŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ"
        + "¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿"
        + "¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿"
        + "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß"
        + "àáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ"
        + "ΔΩμπ–—‘’‚“”„†‡•…L‰‹›⁄ΔΩμπ–—‘’‚“”„†‡•…L‰‹›⁄ΔΩμπ–—‘’‚“”„†‡•…L‰‹›⁄"
        + "     "
}
