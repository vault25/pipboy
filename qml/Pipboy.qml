import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
//import QtQuick.Window 2.2

//import Qt.labs.settings 1.0
//import QtMultimedia 5.15

import QtQuick.VirtualKeyboard 2.0 as VKBD

import "controls" as Controls

import Zax.Core 1.0 as Zax
import Zax.Identity 1.0 as Identity

import Pipboy 1.0 as Pipboy
import Pipboy.GeigerCounter 1.0 as GeigerCounter

Zax.Application {
    id: application

    title: "Pipboy"

    width: 960
    height: 540

    lowPowerTimeout: Pipboy.Preferences.general.lowPowerTimeout

    function reboot() {
        state = Zax.Application.State.Opening
    }

    function factoryReset() {
        state = Zax.Application.State.Error
        currentItem.displayText = "Performing Factory Reset...\nPlease wait.\nDevice will reboot once completed."
        Pipboy.Breakdowns.activeBreakdown = null
    }

    function showPopupMessage(text) {
        popupMessageModel.append({"text": text})
    }

    states: [
        Zax.ApplicationState {
            page: Qt.resolvedUrl("pages/SplashPage.qml")
            state: Zax.Application.State.Opening
            duration: 2000
            skip: Pipboy.Preferences.general.userId ? __skipIntro : false

            onActivated: {
                if (Pipboy.Preferences.general.userId) {
                    Pipboy.SoundEffects.play("boot_sequence")
                    if (Pipboy.Breakdowns.activeBreakdown && Pipboy.Breakdowns.activeBreakdown.activeSolver.type == "reboot") {
                        Pipboy.Breakdowns.activeBreakdown = null
                    }
                }
            }
        },
        Zax.ApplicationState {
            page: Qt.resolvedUrl("pages/IntroPage.qml")
            state: Zax.Application.State.Starting
            duration: 19000
            skip: __skipIntro
        },
        Zax.ApplicationState {
            page: Qt.resolvedUrl("pages/StatusPage.qml")
            state: Zax.Application.State.Operational

            onActivated: {
                Pipboy.Breakdowns.running = true
                GeigerCounter.Counter.enabled = true

                if (__page) {
                    let index = application.pages.findIndex(item => item.title.toLowerCase() === __page.toLowerCase())
                    tabs.currentIndex = index
                    application.stack.replace(application.pages[index].path)
                }
            }

            onDeactivated: {
                application.lowPower = false
                Pipboy.RadioPlayer.stop()
            }
        },
        Zax.ApplicationState {
            page: Qt.resolvedUrl("pages/CrashPage.qml")
            state: Zax.Application.State.Error
        }
    ]

    stack.replaceEnter: Transition {
        SequentialAnimation {
            ScriptAction { script: application.distort = true }
            PauseAnimation { duration: 500 }
        }
    }

    header: TabBar {
        id: tabs

        width: application.width

        Repeater {
            model: application.pages

            TabButton { text: modelData.title }
        }

        onCurrentIndexChanged: {
            if (application.state == Zax.Application.State.Operational) {
                Pipboy.SoundEffects.play("tab")
            }
            application.stack.replace(application.pages[currentIndex].path)
        }

        visible: !application.fullscreen && application.state == Zax.Application.State.Operational
        height: visible ? implicitHeight : 0
    }

    readonly property var pages: [
        { title: "Status", path: Qt.resolvedUrl("pages/StatusPage.qml") },
        { title: "Messages", path: Qt.resolvedUrl("pages/MessagesPage.qml") },
        { title: "Data", path: Qt.resolvedUrl("pages/DataPage.qml") },
        { title: "Map", path: Qt.resolvedUrl("pages/MapsPage.qml") },
        { title: "Radio", path: Qt.resolvedUrl("pages/RadioPage.qml") }
    ]

    Image {
        id: radiationWarningImage
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        width: parent.width * 0.15
        height: width
        source: "qrc:/images/radiation-warning.png"
        fillMode: Image.PreserveAspectFit
        visible: GeigerCounter.Counter.currentRadiation > 50 && !application.currentItem.showingRadiation

        SequentialAnimation on opacity {
            loops: Animation.Infinite;
            running: radiationWarningImage.visible

            PauseAnimation { duration: 1000 }
            NumberAnimation { from: 0.5; to: 1; duration: 10}
            PauseAnimation { duration: 1000 }
            NumberAnimation { from: 1; to: 0.5; duration: 10}
        }

        Label {
            anchors.top: parent.bottom;
            anchors.horizontalCenter: parent.horizontalCenter;
            text: "±%1".arg(Math.round(GeigerCounter.Counter.currentRadiation / 100))
        }
    }

    Image {
        id: objectDetectedImage
        anchors.left: parent.left
        anchors.leftMargin: Pipboy.Style.margins.large * 2
        anchors.bottom: parent.bottom
        anchors.bottomMargin: Pipboy.Style.margins.large * 2

        width: parent.width * 0.15
        height: width
        source: "qrc:/images/object-detected.png"
        fillMode: Image.PreserveAspectFit
        visible: Pipboy.LocationTracker.detected

        SequentialAnimation on opacity {
            loops: Animation.Infinite;
            running: objectDetectedImage.visible

            PauseAnimation { duration: Pipboy.LocationTracker.interval }
            NumberAnimation { from: 0.5; to: 1; duration: 10}
            PauseAnimation { duration: Pipboy.LocationTracker.interval }
            NumberAnimation { from: 1; to: 0.5; duration: 10}
        }

        Label {
            anchors.top: parent.bottom;
            anchors.horizontalCenter: parent.horizontalCenter;
            text: "Object Detected"
        }
    }

    ListView {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.verticalCenter
        anchors.bottom: parent.bottom

        visible: application.state == Zax.Application.State.Operational

        model: ListModel { id: popupMessageModel }

        delegate: Controls.PopupMessage {
            anchors.horizontalCenter: parent ? parent.horizontalCenter : undefined
            text: model.text
        }

        Timer {
            interval: 5000
            running: popupMessageModel.count > 0
            onTriggered: popupMessageModel.remove(0)
        }
    }

    footer: Footer {
        breakdownsVisible: tabs.currentIndex != 0 || !application.currentItem.showingSystem
        messagesVisible: tabs.currentIndex != 1
        keyboardVisible: application.state == Zax.Application.State.Operational
                         && Qt.inputMethod.visible
                         && Pipboy.Breakdowns.activeEffect != "no_input"

        onShowBreakdown: {
            tabs.currentIndex = 0
            application.currentItem.showSystem()
        }
        onShowMessages: {
            tabs.currentIndex = 1
        }
    }

    Label {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: Pipboy.Style.margins.large

        visible: Pipboy.Preferences.development.showLocation

        text: GeigerCounter.Counter.locationInformation
    }

    Connections {
        target: Pipboy.Breakdowns

        function onActiveBreakdownChanged() {
            if (Pipboy.Breakdowns.activeBreakdown && Pipboy.Breakdowns.activeBreakdown.critical) {
                application.state = Zax.Application.State.Error
                currentItem.displayText = Pipboy.Breakdowns.activeBreakdown.reason
            }
        }
    }

    Component.onCompleted: {
        if (Pipboy.Preferences.general.userId) {
            Identity.Self.id = Pipboy.Preferences.general.userId
        }
    }
}
