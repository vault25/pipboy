import QtQuick 2.15
import QtQuick.Controls 2.15

import Pipboy 1.0 as Pipboy

Page {
    id: base

    property bool running: false
    property bool completed: false

    function start() {
        if(!running) {
            completed = false
            d.currentLine = 0
            d.currentStep = -1
            d.currentDelay = 0
            d.completeText = ""
            running = true
            startupText.forceActiveFocus()
        }
    }

    property var texts: [
        { left: "Starting RobCo Industries Pip-OS(R)", right: "", delay: 0 },
        { left: "Please Stand By...", right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "Initializing hardware... ", right: "[OK]", delay: 10 },
        { left: "64k RAM System", right: "", delay: 0 },
        { left: "38911 Bytes Free", right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "Loading kernel... ", right: "[FAIL]", delay: 10 },
        { left: "ERROR! Could not verify kernel integrity!", right: "", delay: 0 },
        { left: "Do you wish to continue? [Y/N]:", right: "Error 0x00B636C6", delay: 10 },
        { left: "Starting kernel... ", right: "[OK]", delay: 10 },
        { left: "", right: "", delay: 0 },
        { left: "Pip-OS(R) v%1".arg(Pipboy.Constants.version), right: "", delay: 0 },
        { left: "Copyright (c) %1 RobCo Industries".arg(Pipboy.Constants.buildYear), right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "Starting input services... ", right: "[WARN]", delay: 10 },
        { left: "WARNING: Keyboard not found!", right: "", delay: 0 },
        { left: "Starting storage services... ", right: "[OK]", delay: 10 },
        { left: "No holotape loaded.", right: "", delay: 0 },
        { left: "Starting communication services... ", right: "[OK]", delay: 10 },
        { left: "Checking for updates... ", right: "[FAIL]", delay: 20 },
        { left: "Could not contact update servers!", right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "WARNING! This version of PipOS is %1 years old.".arg(d.pipOSAge), right: "", delay: 0 },
        { left: "Automatic download of the most recent version of PipOS failed. Please contact the nearest RobCo Industries representative as soon as possible to obtain the latest version of PipOS.", right: "", delay: 0 },
        { left: "", right: "", delay: 0 },
        { left: "Starting graphical display service... ", right: "[OK]", delay: 10 },
        { left: "Load ROM: DEITRIX 286", right: "", delay: 0 }
    ]

    Image {
        anchors.centerIn: parent
        width: parent.width * 0.5
        source: Qt.resolvedUrl("../../images/pipboy-logo.png")
        fillMode: Image.PreserveAspectFit
        opacity: 0.1
    }

    ScrollView {
        id: textScrollView
        anchors.fill: parent
        anchors.margins: 20

        TextArea {
            id: startupText
            text: d.completeText

            cursorPosition: d.completeText.length
            cursorVisible: true

            readOnly: true

            focus: true

            background: Item { }
        }
    }

    Timer {
        running: base.running
        interval: 19
        repeat: true
        onTriggered: {
            if(d.currentDelay > 0) {
                d.currentDelay -= 1;
                return
            }

            d.currentStep += 1
            Pipboy.SoundEffects.play("text_scroll")
            if(d.currentStep >= d.currentText.left.length) {
                if(d.currentLine + 1 >= base.texts.length) {
                    base.running = false
                    base.completed = true
                    return
                }

                let spaces = d.lineWidth - d.currentText.left.length - d.currentText.right.length
                d.completeText += d.currentText.right.padStart(spaces + d.currentText.right.length, " ")

                d.currentLine += 1
                d.currentStep = -1
                d.completeText += "\n"
            } else {
                if(d.currentText.delay > 0 && d.currentStep === d.pausePosition) {
                    d.currentDelay = d.currentText.delay
                }
                d.completeText += d.currentText.left[d.currentStep]
            }
        }
    }

    FontMetrics {
        id: metrics
        font: startupText.font
    }

    QtObject {
        id: d

        property int currentLine: 0
        property int currentStep: -1
        property int currentDelay: 0

        property real lineWidth: Math.floor((textScrollView.width - 40) / metrics.averageCharacterWidth)

        property var currentText: base.texts[currentLine]

        property int pausePosition: currentText.left.length - 2

        property string completeText: ""

        property string pipOSAge: Qt.formatDate(Pipboy.Constants.inGameDate, "yyyy") - Pipboy.Constants.buildYear;
    }

    Component.onCompleted: {
        start()
    }
}
