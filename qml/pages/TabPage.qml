import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "../controls" as Controls

Page {
    id: root

    property alias tabs: tabRepeater.model
    property real tabPosition: 0.5

    property alias currentIndex: tabBar.currentIndex

    property string nameRole: "name"
    property var nameFunction: function (modelData) {
        return modelData[root.nameRole]
    }

    property alias active: loader.active
    property alias source: loader.source
    property alias sourceComponent: loader.sourceComponent

    header: Controls.SubTabBar {
        id: tabBar

        tabPosition: ApplicationWindow.window ? ApplicationWindow.window.contentWidth * root.tabPosition : 0

        height: 30
        y: -10
        visible: ApplicationWindow.window ? !ApplicationWindow.window.fullscreen : false

        Repeater {
            id: tabRepeater

            model: root.tabs

            Controls.SubTabButton {
                text: root.nameFunction(modelData !== undefined ? modelData : model)
            }
        }
    }

    Loader {
        id: loader

        anchors.fill: parent

        asynchronous: true
    }
}
