import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "../controls" as Controls

TabPage {
    tabs: [
        {
            name: "Notes",
            url: Qt.resolvedUrl("../data/Notes.qml")
        },
        {
            name: "Gallery",
            url: Qt.resolvedUrl("../data/Gallery.qml")
        },
        {
            name: "Blueprints",
            url: Qt.resolvedUrl("../data/Blueprints.qml")
        }
    ]

    source: tabs[currentIndex].url
}
