import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Zax.JsonApi 1.0 as JsonApi
import Zax.Identity 1.0 as Identity

import Pipboy 1.0 as Pipboy

import "../controls" as Controls


TabPage {
    id: page

    property QtObject currentChannel: {
        if (currentIndex >= 0 && Pipboy.Messages.channels.length > 0) {
            return Pipboy.Messages.channels[currentIndex]
        }
        return null
    }

    onCurrentChannelChanged: {
        if (currentChannel) {
            currentChannel.lastRead = new Date();
        }
    }

    tabs: Pipboy.Messages.channels
    tabPosition: 0.3

    nameFunction: function (modelData) {
        if (modelData.unreadCount > 0) {
            return "%1 [%2]".arg(modelData.name).arg(modelData.unreadCount)
        } else {
            return modelData.name
        }
    }

    active: {
        return currentChannel != null
    }

    sourceComponent: ColumnLayout {
        ListView {
            id: list

            Layout.fillWidth: true
            Layout.fillHeight: true

            model: JsonApi.ApiModel {
                id: messagesModel

                path: "node/message"

                JsonApi.FilterRule { field: "status"; value: 1 }
                JsonApi.FilterRule { field: "field_channel.id"; value: page.currentChannel.id }

                JsonApi.SortRule { field: "field_in_game_date"; direction: JsonApi.SortRule.Descending }

                JsonApi.IncludeRule { field: "field_device" }

                fields: {
                    "body": "string",
                    "field_in_game_date": "in_game_date",
                    "field_device": "object"
                }
            }

            spacing: Pipboy.Style.margins.medium
            clip: true

            ScrollIndicator.vertical: ScrollIndicator {
                anchors.right: parent.right
                anchors.rightMargin: Pipboy.Style.margins.large
            }

            boundsBehavior: Flickable.StopAtBounds

            delegate: Controls.MessageDelegate {
                width: ListView.view.width

                leftPadding: Pipboy.Style.margins.large + Pipboy.Style.margins.medium
                rightPadding: Pipboy.Style.margins.large + Pipboy.Style.margins.medium
                leftInset: Pipboy.Style.margins.large
                rightInset: Pipboy.Style.margins.large

                flat: true

                title: Qt.formatDateTime(model.fields.field_in_game_date, "yyyy/MM/dd hh:mm:ss")
                sender: (model.fields.field_device ?? {"field_character": "Unknown Device"}).field_character
                message: model.fields.body

                onClicked: background.forceActiveFocus()
            }

            verticalLayoutDirection: ListView.BottomToTop

            data: MouseArea {
                id: background

                anchors.fill: parent
                z: -1

                onClicked: forceActiveFocus()
            }

            Connections {
                target: page.currentChannel

                function onUnreadCountChanged() {
                    currentChannel.lastRead = new Date();
                    messagesModel.query.removeFromCache();
                    messagesModel.refresh();
                }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.preferredHeight: Pipboy.Style.margins.large
            visible: !page.currentChannel.postingEnabled
        }

        ToolBar {
            Layout.fillWidth: true
            Layout.preferredHeight: layout.implicitHeight + Pipboy.Style.margins.large + Pipboy.Style.margins.medium

            visible: page.currentChannel.postingEnabled

            bottomInset: Pipboy.Style.margins.medium

            background: Rectangle {
                color: Pipboy.Style.colors.background

                Rectangle {
                    anchors.fill: parent
                    color: Pipboy.Style.colors.alternativeBackground
                }
            }

            RowLayout {
                id: layout

                anchors.fill: parent
                anchors.margins: Pipboy.Style.margins.large
                anchors.topMargin: Pipboy.Style.margins.medium

                TextArea {
                    id: messageField

                    Layout.fillWidth: true

                    property bool sending: false

                    enabled: Pipboy.Breakdowns.activeEffect != "no_input" && !sending

                    background: Rectangle {
                        color: Pipboy.Style.colors.background
                        border.width: Pipboy.Style.sizes.borderWidth
                        border.color: Pipboy.Style.colors.primary
                    }

                    onActiveFocusChanged: {
                        if (activeFocus) {
                            ApplicationWindow.window.fullscreen = true
                        } else {
                            ApplicationWindow.window.fullscreen = false
                        }
                    }
                }

                Button {
                    id: sendButton

                    Layout.alignment: Qt.AlignTop

                    text: "Send"
                    flat: true

                    enabled: !messageField.sending

                    onClicked: {
                        messageField.sending = true
                        Pipboy.Messages.sendMessage(page.currentChannel.id, messageField.text)
                    }
                }

                Connections {
                    target: Pipboy.Messages

                    function onMessagePosted() {
                        messagesModel.query.removeFromCache();
                        messagesModel.refresh();
                        messageField.text = ""
                        messageField.sending = false
                    }

                    function onMessageError() {
                        showPopupMessage("Failed posting message: %1".arg(errorString))
                        messageField.sending = false
                    }
                }
            }
        }
    }
}
