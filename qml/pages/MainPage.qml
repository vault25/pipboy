import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import QtQuick.VirtualKeyboard 2.0

import Pipboy 1.0 as Pipboy

import "../controls" as Controls

Page {
    property alias stack: stackView;

    header: TabBar {
        id: tabs
        Layout.fillWidth: true
        TabButton { text: "Status"; onClicked: stackView.replace(Qt.resolvedUrl("StatusPage.qml")) }
        TabButton { text: "Messages"; onClicked: stackView.replace(Qt.resolvedUrl("MessagesPage.qml")) }
        TabButton { text: "Data"; onClicked: stackView.replace(Qt.resolvedUrl("DataPage.qml")) }
        TabButton { text: "Map"; onClicked: stackView.replace(Qt.resolvedUrl("MapsPage.qml")) }
        TabButton { text: "Radio"; onClicked: stackView.replace(Qt.resolvedUrl("RadioPage.qml")) }

        onCurrentIndexChanged: {
            ApplicationWindow.window.distort = true;
            Pipboy.SoundEffects.play("tab")
        }

        visible: ApplicationWindow.window ? !ApplicationWindow.window.fullscreen : false
    }

    ColumnLayout {
        anchors.fill: parent

        StackView {
            id: stackView

            Layout.fillWidth: true
            Layout.fillHeight: true

            initialItem: Qt.resolvedUrl("StatusPage.qml")
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: notificationsLayout.height + (notificationsLayout.height > 0 ? Pipboy.Style.margins.large + Pipboy.Style.margins.small : 0)
            color: "black"
            visible: !Qt.inputMethod.visible

            Rectangle {
                anchors {
                    fill: parent
                }
                color: Pipboy.Style.colors.primary;
                opacity: 0.25
                radius: Pipboy.Style.sizes.cornerRadius
            }

            Column {
                id: notificationsLayout

                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                    margins: Pipboy.Style.margins.large
                }
                spacing: 0

                RowLayout {
                    width: parent.width
                    visible: messageWatcher.unreadCount > 0 ? 1 : 0

                    Label {
                        Layout.fillWidth: true;
                        displayText: "%1 New Message(s)".arg(messageWatcher.unreadCount)
                    }
                    Button {
                        text: "Show";
                        flat: true;
                        onClicked: {
                            stackView.replace(Qt.resolvedUrl("MessagesPage.qml"))
                            tabs.currentIndex = 0;
                            stackView.currentItem.showMessages();
                        }
                    }
                }

                RowLayout {
                    width: parent.width
                    property var breakdown: Pipboy.Breakdowns.activeBreakdown
                    visible: breakdown != null

                    Label {
                        Layout.fillWidth: true;
                        displayText: "System Error: %1".arg(parent.breakdown ? parent.breakdown.subsystem : "")
                    }
                    Button {
                        text: "Show";
                        flat: true;
                        onClicked: {
                            stackView.replace(Qt.resolvedUrl("StatusPage.qml"))
                            tabs.currentIndex = 0;
                            stackView.currentItem.showSystem();
                        }
                    }
                }
            }

            Pipboy.MessageWatcher {
                id: messageWatcher
                lastRead: Pipboy.Preferences.general.lastDisplayedMessage
                property int lastUnreadCount
                onUnreadCountChanged: {
                    if (unreadCount > lastUnreadCount) {
                        Pipboy.SoundEffects.play("message")
                    }
                    lastUnreadCount = unreadCount
                }
            }
        }

        InputPanel {
            visible: Qt.inputMethod.visible && !Pipboy.Breakdowns.activeVisual == "no_input"
        }
    }
}
