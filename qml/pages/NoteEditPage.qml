import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Pipboy 1.0 as Pipboy

import "../controls" as Controls

Page {
    id: page

    property string text

    signal close()

    StackView.onActivated: ApplicationWindow.window.fullscreen = true
    StackView.onDeactivated: ApplicationWindow.window.fullscreen = false

    header: ToolBar {
        height: title.implicitHeight + Pipboy.Style.margins.large + Pipboy.Style.margins.small

        leftPadding: Pipboy.Style.margins.large
        rightPadding: Pipboy.Style.margins.large
        topPadding: Pipboy.Style.margins.large
        bottomPadding: Pipboy.Style.margins.small

        Button {
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter

            text: "Cancel"
            flat: true

            onClicked: page.close()
        }

        Label {
            id: title

            anchors.fill: parent

            text: page.title
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }

        Button {
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter

            text: "Save"
            flat: true

            onClicked: {
                page.text = textArea.text
                page.close()
            }
        }
    }

    Flickable {
        anchors.fill: parent
        anchors.leftMargin: Pipboy.Style.margins.large
        anchors.rightMargin: Pipboy.Style.margins.large

        ScrollIndicator.vertical: ScrollIndicator {
            anchors.right: parent.right
            anchors.rightMargin: Pipboy.Style.margins.large
        }

        TextArea.flickable: TextArea {
            id: textArea

            width: page.width

            text: page.text

            focus: true

//             TextArea.flickable: parent
        }
    }
}

