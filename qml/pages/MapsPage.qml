import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Zax.JsonApi 1.0 as JsonApi
import Zax.Identity 1.0 as Identity

import Pipboy 1.0 as Pipboy

import "../controls" as Controls

TabPage {
    id: root

    property string currentMapId: {
        if (currentIndex >= 0 && tabs.count > 0) {
            return tabs.data(tabs.index(currentIndex, 0), JsonApi.ApiModel.IdRole)
        }
        return ""
    }

    tabs: JsonApi.ApiModel {
        path: "node/map";

        JsonApi.FilterRule { field: "status"; value: 1 }
        JsonApi.ClientFilterRule {
            field: "field_devices";
            value: Identity.Self.id
            empty: JsonApi.ClientFilterRule.AcceptEmpty
        }

        JsonApi.SortRule { fields: ["field_weight", "title"] }
    }
    tabPosition: 0.7
    nameRole: "title"

    active: currentMapId !== ""

    sourceComponent: Controls.ImageViewer {
        id: image

        source: "image://jsonapi/node/map/%1?field=field_image".arg(root.currentMapId)
        fillMode: Image.PreserveAspectCrop

        property Item currentMapMarker

        MouseArea {
            anchors.fill: parent
            onClicked: currentMapMarker = null
        }

        Repeater {
            model: JsonApi.ApiModel {
                path: "node/map_marker"

                fields: {
                    "field_x_position": "float",
                    "field_y_position": "float"
                }

                JsonApi.FilterRule { field: "status"; value: 1 }
                JsonApi.FilterRule { field: "field_map.id"; value: root.currentMapId }
            }

            Button {
                id: mapMarker

                x: model.fields.field_x_position
                y: model.fields.field_y_position
                z: checked ? 99 : 0

                scale: checked ? 1.25 : 1

                checked: image.currentMapMarker == this

                onClicked: {
                    image.currentMapMarker = mapMarker
                    Pipboy.SoundEffects.play("map_select")
                }

                contentItem: Image {
                    source: "image://jsonapi/node/map_marker/%1".arg(model.id)
                }

                background: Item { }

                property real bottomEdge: {
                    if (!checked) {
                        return 0.0
                    }

                    return mapToItem(root, 0.0, height + label.height).y
                }

                Label {
                    id: label

                    anchors.horizontalCenter: parent.horizontalCenter

                    y: parent.bottomEdge > root.height - Pipboy.Style.margins.large ? -height : parent.height

                    padding: Pipboy.Style.margins.small

                    visible: parent.checked
                    background: Rectangle { color: Pipboy.Style.colors.tertiary }

                    displayText: model.title
                }
            }
        }
    }
}
