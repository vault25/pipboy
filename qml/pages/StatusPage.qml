import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Pipboy 1.0 as Pipboy

import "../status" as Status

TabPage {
    property bool showingSystem: currentIndex == 2
    property bool showingRadiation: currentIndex == 1

    function showSystem() {
        currentIndex = 2
    }

    tabs: [
        { name: "Time", url: Qt.resolvedUrl("../status/Time.qml") },
        { name: "Radiation", url: Qt.resolvedUrl("../status/Radiation.qml") },
        { name: "System", url: Qt.resolvedUrl("../status/System.qml") },
        { name: "Settings", url: Qt.resolvedUrl("../status/Settings.qml") }
    ]

    tabPosition: 0.125

    source: tabs[currentIndex].url
}

