import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.9

import Zax.JsonApi 1.0 as JsonApi
import Zax.Identity 1.0 as Identity

import Pipboy 1.0 as Pipboy

import "../controls"

Page {
    id: page

    ListWithDetails {
        id: list

        anchors.fill: parent

        model: JsonApi.ApiModel {
            path: "node/radio_stream"
            fields: {
                "field_stream": "url",
                "body": "string",
            }

            JsonApi.SortRule { fields: ["field_weight", "title"] }
            JsonApi.FilterRule { field: "status"; value: 1 }
            JsonApi.ClientFilterRule { field: "field_devices"; value: Identity.Self.id; empty: JsonApi.ClientFilterRule.AcceptEmpty }
        }

        nameRole: "title"

        hasActiveItem: true
        activeIndex: {
            for (var i = 0; i < model.count; ++i) {
                var url = model.get(i).fields.field_stream
                if (Pipboy.RadioPlayer.source == url) {
                    return i;
                }
            }
            return -1;
        }

        property var currentStream: {
            if (currentIndex < 0 || currentIndex >= model.count) {
                return {
                    "id": "",
                    "title": "",
                    "fields": {
                        "field_stream": "",
                        "body": ""
                    }
                };
            }

            return model.get(currentIndex)
        }

        header: RowLayout {
            width: list.view.width
            spacing: 0

            Button {
                Layout.preferredWidth: 80
                Layout.bottomMargin: Pipboy.Style.margins.small

                property string link: list.currentStream.fields.field_stream

                text: Pipboy.RadioPlayer.playing && Pipboy.RadioPlayer.source == link ? "Pause" : "Play"
                onClicked: {
                    if (Pipboy.RadioPlayer.playing && Pipboy.RadioPlayer.source == link) {
                        Pipboy.RadioPlayer.stop()
                        Pipboy.SoundEffects.play("radio_off")
                    } else {
                        Pipboy.RadioPlayer.source = link
                        print(link)
                        Pipboy.RadioPlayer.play()
                        Pipboy.SoundEffects.play("radio_on")
                    }
                }
                enabled: {
                    if(!list.currentItem) {
                        return false
                    }

                    return true
                }
                flat: true
            }

            Item { Layout.fillWidth: true; Layout.preferredHeight: 1; Layout.minimumWidth: 10    }

            Label {
                Layout.fillHeight: true;
                Layout.bottomMargin: Pipboy.Style.margins.small
                displayText: "VOL";
                verticalAlignment: Text.AlignVCenter
            }

            Button {
                Layout.fillHeight: true;
                Layout.leftMargin: Pipboy.Style.margins.small;
                Layout.bottomMargin: Pipboy.Style.margins.small
                text: "-";
                flat: true;
                onClicked: controller.reduceVolume()
            }

            TextField {
                Layout.preferredWidth: height * 2;
                Layout.fillHeight: true;
                Layout.bottomMargin: Pipboy.Style.margins.small
                text: controller.volume;
                padding: Pipboy.Style.margins.small;
                readOnly: true
            }

            Button {
                Layout.fillHeight: true;
                Layout.bottomMargin: Pipboy.Style.margins.small
                text: "+";
                flat: true;
                onClicked: controller.increaseVolume()
            }
        }

        detailsActive: currentIndex >= 0 && currentIndex < model.count
        details: ColumnLayout {
            spacing: Pipboy.Style.margins.large

            Image {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.maximumHeight: page.height * 0.4

                fillMode: Image.PreserveAspectFit

                source: "image://jsonapi/node/radio_stream/%1?field=field_image".arg(list.currentStream.id)
            }

            Label {
                Layout.fillWidth: true
                Layout.fillHeight: true

                text: list.currentStream.fields.body ? list.currentStream.fields.body : "No information provided."

                wrapMode: Text.WordWrap
            }
        }

        Pipboy.VolumeController {
            id: controller
            mediaPlayer: Pipboy.RadioPlayer.mediaPlayer
        }

        Connections {
            target: Pipboy.RadioPlayer.mediaPlayer

            function onError(error, errorString) {
                showPopupMessage("Unable to play radio: " + errorString)
            }
        }
    }
}
