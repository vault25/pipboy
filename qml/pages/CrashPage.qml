import QtQuick 2.15
import QtQuick.Controls 2.15

import Zax.Core 1.0 as Zax

import Pipboy 1.0 as Pipboy

Page {
    id: page

    property alias displayText: label.displayText

    function reboot() {
        ApplicationWindow.window.state = Zax.Application.State.Opening
    }

    Image {
        anchors.centerIn: parent
        width: parent.width * 0.5
        source: Qt.resolvedUrl("../../images/pipboy-logo.png")
        fillMode: Image.PreserveAspectFit
        opacity: 0.1
    }

    Label {
        id: label
        anchors.fill: parent
        anchors.margins: Pipboy.Style.margins.large
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap
    }

    Timer {
        id: rebootTimer
        interval: 10000
        running: true
        onTriggered: {
            page.reboot()
        }
    }
}
