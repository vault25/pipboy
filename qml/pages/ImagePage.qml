import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Pipboy 1.0 as Pipboy

import "../controls" as Controls

Page {
    id: page

    property url source
    property string description

    signal close()

    StackView.onActivated: ApplicationWindow.window.fullscreen = true
    StackView.onDeactivated: ApplicationWindow.window.fullscreen = false

    header: ToolBar {
        height: title.implicitHeight + Pipboy.Style.margins.large + Pipboy.Style.margins.small

        leftPadding: Pipboy.Style.margins.large
        rightPadding: Pipboy.Style.margins.large
        topPadding: Pipboy.Style.margins.large
        bottomPadding: Pipboy.Style.margins.small

        Label {
            id: title

            anchors.fill: parent

            text: page.title
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }

        Button {
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter

            text: "Cls"
            flat: true

            onClicked: page.close()
        }
    }

    Controls.ImageViewer {
        anchors.fill: parent
        source: page.source
        fillMode: Image.PreserveAspectFit

        TapHandler {
            onTapped: page.close()
        }
    }

    footer: Label {
        leftPadding: Pipboy.Style.margins.large
        rightPadding: Pipboy.Style.margins.large
        bottomPadding: Pipboy.Style.margins.large
        topPadding: Pipboy.Style.margins.small

        text: page.description
        wrapMode: Text.Wrap
        horizontalAlignment: Text.AlignHCenter

        visible: text != ""
    }
}
