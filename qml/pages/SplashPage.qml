import QtQuick 2.15
import QtQuick.Controls 2.15

import QtQuick.VirtualKeyboard 2.0 as VKBD

import Zax.Identity 1.0 as Identity

import Pipboy 1.0 as Pipboy

import ".."

Page {
    id: page

    Image {
        anchors.centerIn: parent
        width: parent.width * 0.5
        source: Qt.resolvedUrl("../../images/pipboy-logo.png")
        fillMode: Image.PreserveAspectFit
    }

    StartupDialog {
        id: startupDialog

        onAccepted: ApplicationWindow.window.currentIndex += 1
    }

    Connections {
        target: Identity.Self

        onChanged: {
            if (!Identity.Self.valid) {
                startupDialog.open()
            }
        }

        onError: {
            if (!Identity.Self.valid) {
                startupDialog.open()
            }
        }
    }

    Component.onCompleted: {
        if (!Pipboy.Preferences.general.userId) {
            startupDialog.open()
        }
    }

    VKBD.InputPanel {
        visible: Qt.inputMethod.visible

        width: Math.min(page.width, 850)
        y: page.height - height
    }
}
