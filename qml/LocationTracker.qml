import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import QtPositioning 5.15
import QtMultimedia 5.15
import QtSensors 5.15 as QtSensors

import Zax.JsonApi 1.0 as JsonApi
import Zax.Location 1.0 as Location
import Zax.Identity 1.0 as Identity

import "." as Pipboy

pragma Singleton;

Item {
    id: root

    property bool enabled: Pipboy.Preferences.general.trackerEnabled

    property bool detected: false

    property bool locationCount: query.entries.length

    readonly property bool effectiveEnabled: enabled && locationCount > 0

    property int interval: 350
    property int direction
    property real volume: 1.0

    Location.ProximitySensor {
        id: sensor

        origin: Location.DeviceLocationSource {
            updateInterval: root.effectiveEnabled ? 100 : 0
            minimumAccuracy: 10
        }

        updateInterval: root.effectiveEnabled ? 100 : 0

        onDetected: {
            let distanceFactor = distance / location.triggerDistance
            root.interval = Math.max(350, distanceFactor * 5000)
            root.detected = true

            root.direction = azimuth < 180 ? azimuth : (azimuth > 180 ? azimuth % 180 : 180)

            resetTimer.restart()
        }
    }

    Timer {
        id: soundEffectTimer

        running: root.detected
        repeat: true
        triggeredOnStart: true

        interval: root.interval

        onTriggered: Pipboy.SoundEffects.play("tracker", root.volume)
    }

    Timer {
        id: resetTimer
        interval: 10000

        onTriggered: root.detected = false
    }

    JsonApi.Query {
        id: query

        path: "/node/tracker_location"

        refreshInterval: 60000
        refreshOnChange: true

        JsonApi.FilterRule { field: "status"; value: 1 }
        JsonApi.ClientFilterRule { field: "field_devices"; value: Identity.Self.id; empty: JsonApi.ClientFilterRule.RejectEmpty }

        Component.onCompleted: run()
    }

    Instantiator {
        model: query.entries

        Location.LocationSource {
            identifier: modelData.id
            coordinate: QtPositioning.coordinate(modelData.field_gps_location.lat, modelData.field_gps_location.lon)
            triggerDistance: 500
        }

        onObjectAdded: {
            sensor.addLocation(object)
        }
        onObjectRemoved: {
            sensor.removeLocation(object.identifier)
        }
    }

    QtSensors.Compass {
        id: compass
        active: root.effectiveEnabled
        onReadingChanged: {
            let deviceDirection = reading.azimuth + 90
            deviceDirection = deviceDirection < 180 ? deviceDirection : (deviceDirection > 180 ? deviceDirection % 180 : 180)
            root.volume = Math.max(0.2, Math.abs(root.direction - deviceDirection) / 180)
        }
    }
}
