import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import QtMultimedia 5.15

import Pipboy.Breakdowns 1.0

import "."

pragma Singleton;

BreakdownHandler {
    id: handler

    overallChance: 0.33
    minimumInterval: 1000 * 60 * 10
    maximumInterval: 1000 * 60 * 30

    onActiveEffectChanged: {
        Style.activeEffect = activeEffect

        if (activeEffect == "noise_sound") {
            const options = [
                "qrc:/sounds/UI_Pipboy_Radio_StaticTuning_01_LP.wav",
                "qrc:/sounds/UI_Pipboy_Radio_StaticTuning_02_LP.wav",
                "qrc:/sounds/UI_Pipboy_Radio_StaticTuning_03_LP.wav"
            ]
            var index = Math.floor(Math.random() * options.length)
            handler.noiseEffect.source = options[index]
            handler.noiseEffect.play()
        } else {
            handler.noiseEffect.stop()
        }
    }

    property SoundEffect noiseEffect: SoundEffect {
        loops: SoundEffect.Infinite
    }

    Breakdown {
        chance: 1
        subsystem: "System"
        code: "0xA953D0DF"
        reason: "Kernel verfication failure! This PipBoy is not running genuine RobCo PipBoy software and cannot continue operating."
        critical: true

        RebootSolver { }
    }

    Breakdown {
        chance: 20
        subsystem: "System"
        code: "0xFFFFF710"
        reason: "Memory Corruption Detected. Remove interference then reboot system."

        effects: [
            "text_corruption",
            "noise"
        ]

        RebootSolver { chance: 0.5 }
        TimeSolver {
            chance: 0.5
            minimumDuration: 10000
            maximumDuration: 30000
        }
    }

    Breakdown {
        chance: 40
        subsystem: "Display"
        code: "0x10B1DFF4"
        reason: "Display connection error. Check display wiring for faults."

        effects: [
            "sine",
            "noise"
        ]

        ShakeSolver {
            chance: 0.5
            axis: ShakeSolver.AllAxis
            sensitivity: 10
            cooldownDuration: 500
            peakGoal: 5
        }
    }

    Breakdown {
        chance: 40
        subsystem: "Display"
        code: "0xC971A6BA"
        reason: "Display synchronisation failure. Reinitialize display unit."

        effects: [
            "scroll",
            "sine"
        ]

        TimeSolver { chance: 0.5; minimumDuration: 10000; maximumDuration: 60000 }
        ShakeSolver {
            chance: 0.5
            axis: ShakeSolver.AllAxis
            sensitivity: 10
            cooldownDuration: 500
            peakGoal: 5
        }
    }

    Breakdown {
        chance: 40
        subsystem: "Display"
        code: "0x0E449FAD"
        reason: "Beam decoherence detected. Recalibrate cathode-ray beam tube."

        effects: [
            "pixelize"
        ]

        ShakeSolver {
            chance: 0.5
            axis: ShakeSolver.AllAxis
            sensitivity: 10
            cooldownDuration: 500
            peakGoal: 5
        }
    }

    Breakdown {
        chance: 20
        subsystem: "Input"
        code: "0x00B636C6"
        reason: "No input device found. Connect keyboard."

        effects: [ "no_input" ]

        RebootSolver { chance: 0.25 }
        ShakeSolver {
            chance: 0.75
            axis: ShakeSolver.AllAxis
            sensitivity: 10
            cooldownDuration: 500
            peakGoal: 5
        }
    }

    Breakdown {
        chance: 40
        subsystem: "Storage"
        code: "0xF141A013"
        reason: "No data storage detected. Check tape drive connection."

        effects: [ "no_editing" ]

        ShakeSolver {
            chance: 0.5
            axis: ShakeSolver.AllAxis
            sensitivity: 10
            cooldownDuration: 500
            peakGoal: 5
        }
    }

    Breakdown {
        chance: 20
        subsystem: "Storage"
        code: "0x07F6BAAC"
        reason: "Bad Data. Cannot read."

        effects: [
            "text_corruption",
            "no_editing"
        ]

        TimeSolver { chance: 0.5; minimumDuration: 10000; maximumDuration: 60000 }
        RebootSolver { chance: 0.5; }
    }

    Breakdown {
        chance: 20
        subsystem: "Comms"
        code: "0x6A1D73B4"
        reason: "Magnetic interference detected. Degauss communication antenna."

        effects: [
            "noise_sound"
        ]

        TimeSolver { chance: 0.5; minimumDuration: 10000; maximumDuration: 60000 }
        ShakeSolver {
            chance: 0.5
            axis: ShakeSolver.AllAxis
            sensitivity: 10
            cooldownDuration: 500
            peakGoal: 5
        }
    }
}
