import QtQuick 2.7
import QtQuick.Controls 2.2

Item {
    id: base

    property alias speed: timer.interval;
    property var entries: [];
    property string separator: " +++ ";
    property int characterCount: Math.floor(width / metrics.averageCharacterWidth);

    Label { id: label; anchors.fill: parent; text: d.section }

    Timer {
        id: timer;
        interval: 300;
        running: true
        repeat: true
        onTriggered: d.startIndex = (d.startIndex + 1) % d.fullString.length;
    }

    QtObject {
        id: d

        property int startIndex: 0;
        property int endIndex: (startIndex + base.characterCount) % fullString.length;
        property string fullString: base.entries.join(base.separator) + base.separator;
        property string section: {
            if(startIndex < endIndex) {
                return fullString.substring(startIndex, endIndex);
            } else {
                var part1 = fullString.substring(startIndex);
                var part2 = fullString.substring(0, endIndex);
                return part1 + part2;
            }
        }
    }

    FontMetrics { id: metrics; font: label.font }
}
