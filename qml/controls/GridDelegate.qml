import QtQuick 2.15
import QtQuick.Controls 2.15

import Pipboy 1.0 as Pipboy

ItemDelegate {
    id: control

    implicitWidth: Math.max(background ? background.implicitWidth : 0,
                            contentItem.implicitWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(background ? background.implicitHeight : 0,
                             Math.max(contentItem.implicitHeight,
                                      indicator ? indicator.implicitHeight : 0) + topPadding + bottomPadding)
    baselineOffset: contentItem.y + contentItem.baselineOffset

    padding: Pipboy.Style.margins.medium
    spacing: Pipboy.Style.margins.medium

    font: highlighted ? Pipboy.Style.fonts.normalBold : Pipboy.Style.fonts.normal

    contentItem: Label {
        leftPadding: Pipboy.Style.margins.medium
        rightPadding: Pipboy.Style.margins.medium

        displayText: control.text
        font: control.font
        color: control.highlighted || control.down ? Pipboy.Style.colors.primaryText : Pipboy.Style.colors.primary
        elide: Text.ElideRight
        visible: control.text
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter

        background: Rectangle {
            color: Pipboy.Style.colors.background
            opacity: 0.5
        }
    }

    background: Rectangle {
        implicitWidth: 200
        implicitHeight: 200
        color: control.down ? Pipboy.Style.colors.secondary : !control.highlighted ? Pipboy.Style.colors.background : Style.colors.primary

        Rectangle {
            anchors.fill: parent
            anchors.margins: Pipboy.Style.sizes.borderWidth / 2
            color: "transparent"
            border.color: Pipboy.Style.colors.primary
            border.width: Pipboy.Style.sizes.borderWidth
        }
    }
}

