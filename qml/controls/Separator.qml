import QtQuick 2.15

import Pipboy 1.0 as Pipboy

Rectangle {
    implicitWidth: Pipboy.Style.sizes.borderWidth
    implicitHeight: Pipboy.Style.sizes.borderWidth
    color: Pipboy.Style.colors.primary
}
