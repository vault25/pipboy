import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import Pipboy 1.0 as Pipboy

Item {
    id: base

    property var window: ApplicationWindow.window

    property alias view: view
    property alias model: view.model
    property alias delegate: view.delegate
    property alias header: view.header
    property alias footer: view.footer
    property alias currentIndex: view.currentIndex
    property alias currentItem: view.currentItem

    property bool hasActiveItem: false
    property int activeIndex: -1
    readonly property var activeItem: view.itemAt(0, activeIndex * view.delegate.height)

    property string nameRole: "name"

    property alias viewVisible: view.visible

    property alias detailsActive: detailsLoader.active

    property alias details: detailsLoader.sourceComponent

    RowLayout {
        anchors.fill: parent
        ListView {
            id: view
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width / 2
            clip: true
            boundsBehavior: Flickable.StopAtBounds

            currentIndex: 0

            ScrollIndicator.vertical: ScrollIndicator {
                anchors.right: parent.right
                anchors.rightMargin: Pipboy.Style.margins.medium
            }

            delegate: ItemDelegate {
                id: delegate

                width: ListView.view.width

                property var modelData: model

                leftPadding: base.hasActiveItem ? Pipboy.Style.margins.large : 0

                highlighted: ListView.isCurrentItem
                text: model[base.nameRole]

                onClicked: {
                    Pipboy.SoundEffects.play("list_item")
                    ListView.view.currentIndex = index
                }

                Rectangle {
                    anchors {
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                        leftMargin: Pipboy.Style.margins.medium
                    }
                    width: Pipboy.Style.margins.medium
                    height: width

                    color: parent.ListView.isCurrentItem ? Pipboy.Style.colors.background : Pipboy.Style.colors.primary

                    visible: base.hasActiveItem && base.activeIndex == index
                }
            }
        }

        Loader {
            id: detailsLoader
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width / 2
        }
    }
}

