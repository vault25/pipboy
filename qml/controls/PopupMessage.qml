import QtQuick 2.7
import QtQuick.Controls 2.2

import Pipboy 1.0 as Pipboy

ItemDelegate {

    background: Rectangle {
        color: Pipboy.Style.colors.background
        border.color: Pipboy.Style.colors.primary
        border.width: Pipboy.Style.sizes.borderWidth
    }
}
