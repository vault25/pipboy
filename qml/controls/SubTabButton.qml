import QtQuick 2.15
import QtQuick.Controls 2.15

import Pipboy 1.0 as Pipboy

TabButton {
    id: control

    property Item tabBar: parent && parent.parent && parent.parent.tabBar !== undefined ? parent.parent.tabBar : null

    width: tabBar ? label.width : undefined

    onClicked: Pipboy.SoundEffects.play("tab")

    contentItem: Item {
        Label {
            id: label
            anchors.centerIn: parent
            leftPadding: Pipboy.Style.margins.small
            rightPadding: Pipboy.Style.margins.small
            text: Pipboy.Style.displayText(control.text)
            font: control.font
            elide: Text.ElideRight
            opacity: control.tabBar != null && !control.checked ? 0.3 : 1.0
            color: Pipboy.Style.colors.primary
            background: Rectangle { color: Pipboy.Style.colors.background }
        }
    }

    background: Item { }
}
