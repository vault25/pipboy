import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Flickable {
    id: flick

    property alias source: image.source
    property alias fillMode: image.fillMode

    property real minZoom: 0.1
    property real maxZoom: 4
    property real zoomStep: 0.1

    readonly property alias currentZoom: image.scale

    default property alias _imageChildren: image.data

    function fitToScreen() {
        let s = 0;

        if (flick.fillMode == Image.Fill || flick.fillMode == Image.PreserveAspectCrop) {
            s = Math.max(flick.width / image.width, flick.height / image.height)
        } else {
            s = Math.min(flick.width / image.width, flick.height / image.height)
        }

        image.scale = s;
        flick.minZoom = s;
        image.prevScale = scale
        flick.returnToBounds();
    }

    function zoomIn() {
        image.scale = Math.min(Math.max(image.scale * (1.0 + zoomStep), minZoom), maxZoom)
        flick.returnToBounds();
    }

    function zoomOut() {
        image.scale = Math.min(Math.max(image.scale * (1.0 - zoomStep), minZoom), maxZoom)
        flick.returnToBounds();
    }

    contentWidth: Math.max(image.width * image.scale, flick.width)
    contentHeight: Math.max(image.height * image.scale, flick.height)

    boundsBehavior: Flickable.StopAtBounds
    clip: true

    ScrollIndicator.vertical: ScrollIndicator {
        anchors.horizontalCenter: parent.horizontalCenter
    }
    ScrollIndicator.horizontal: ScrollIndicator {
        anchors.verticalCenter: parent.verticalCenter
    }

    Image {
        id: image

        property real prevScale: 1.0;

        asynchronous: true
        cache: false
        smooth: true

        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit

        transformOrigin: Item.Center
        onScaleChanged: {
            if ((width * scale) > flick.width) {
                var xoff = (flick.width / 2 + flick.contentX) * scale / prevScale;
                flick.contentX = xoff - flick.width / 2
            }
            if ((height * scale) > flick.height) {
                var yoff = (flick.height / 2 + flick.contentY) * scale / prevScale;
                flick.contentY = yoff - flick.height / 2
            }
            prevScale = scale;
        }
        onStatusChanged: {
            if (status === Image.Ready) {
                flick.fitToScreen();
            }
        }

        WheelHandler {
            acceptedDevices: PointerDevice.Mouse | PointerDevice.TouchPad
            onWheel: {
                if(event.angleDelta.y < 0) {
                    flick.zoomOut()
                } else if(event.angleDelta.y > 0) {
                    flick.zoomIn()
                }
            }
        }

        PinchHandler {
            minimumRotation: 0
            maximumRotation: 0

            minimumScale: flick.minZoom
            maximumScale: flick.maxZoom

            onActiveChanged: {
                if (active) {
                    flick.interactive = false
                } else {
                    flick.interactive = true
                    flick.returnToBounds()
                }
            }
        }
    }
}
