import QtQuick 2.15
import QtQuick.Controls 2.15

import Pipboy 1.0 as Pipboy

TabBar {
    id: control

    property int tabPosition: 0

    spacing: 1
    contentHeight: 40

    bottomPadding: 0

    contentItem: Item {
        ListView {
            //x: control.position
            width: parent.width

            property Item tabBar: control

            model: control.contentModel
            currentIndex: control.currentIndex

            spacing: control.spacing
            orientation: ListView.Horizontal
            boundsBehavior: Flickable.StopAtBounds
            flickableDirection: Flickable.AutoFlickIfNeeded
            snapMode: ListView.SnapToItem

            highlightMoveDuration: 0
            highlightRangeMode: ListView.StrictlyEnforceRange
            preferredHighlightBegin: currentItem ? control.tabPosition - currentItem.width / 2 : 40
            preferredHighlightEnd: currentItem ? control.tabPosition - currentItem.width / 2 : width - 40
        }
    }

    background: Rectangle {
        color: Pipboy.Style.colors.background
    }
}

