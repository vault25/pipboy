import QtQuick 2.15
import QtQuick.Controls 2.15

import Pipboy 1.0 as Pipboy

ItemDelegate {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding)

    property string title
    property string sender
    property string message
    property bool flat: false

    leftPadding: Pipboy.Style.margins.large
    rightPadding: Pipboy.Style.margins.large
    topPadding: Pipboy.Style.margins.medium
    bottomPadding: Pipboy.Style.margins.medium

    background: Item {
        Rectangle {
            anchors.fill: parent
            color: Pipboy.Style.colors.alternativeBackground;
            visible: !control.flat
        }
    }

    contentItem: Item {
        implicitWidth: 500
        implicitHeight: {
            let result = 0
            if (control.sender || control.title) {
                result += Math.max(timeField.implicitHeight, senderField.implicitHeight)
            }

            if (control.message) {
                result += Pipboy.Style.margins.small
                result += messageField.implicitHeight
            }

            return result
        }

        Label {
            id: senderField
            anchors.left: parent.left
            displayText: control.sender

            font: Pipboy.Style.fonts.small
            color: Pipboy.Style.colors.secondary
        }

        Label {
            id: timeField
            anchors.left: senderField.right
            anchors.leftMargin: Pipboy.Style.margins.medium
            displayText: control.title

            font: Pipboy.Style.fonts.small
            color: Pipboy.Style.colors.tertiary
        }

        Label {
            id: messageField
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            displayText: control.message
            wrapMode: Text.WordWrap
            textFormat: Text.PlainText
        }
    }
}
