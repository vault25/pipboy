import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import QtQuick.VirtualKeyboard 2.0 as VKBD

import Pipboy 1.0 as Pipboy

import "controls" as Controls

ColumnLayout {
    id: root

    property bool breakdownsVisible: true
    property bool messagesVisible: true
    property bool keyboardVisible: false

    signal showBreakdown()
    signal showMessages()

    spacing: 0

    Rectangle {
        Layout.fillWidth: true
        Layout.preferredHeight: Pipboy.Style.margins.small
        color: Pipboy.Style.colors.background
        visible: systemNotification.visible || messageNotification.visible || keyboard.visible
    }

    Controls.MessageDelegate {
        id: systemNotification

        Layout.fillWidth: true
        Layout.leftMargin: Pipboy.Style.margins.large
        Layout.rightMargin: Pipboy.Style.margins.large

        visible: Pipboy.Breakdowns.activeBreakdown && root.breakdownsVisible && !ApplicationWindow.window.fullscreen

        message: "System Error: %1".arg(Pipboy.Breakdowns.activeBreakdown ? Pipboy.Breakdowns.activeBreakdown.subsystem : "")

        Button {
            anchors.right: parent.right
            anchors.rightMargin: Pipboy.Style.margins.small
            anchors.verticalCenter: parent.verticalCenter
            text: "Show"
            flat: true

            onClicked: root.showBreakdown()
        }
    }

    Controls.MessageDelegate {
        id: messageNotification

        Layout.fillWidth: true
        Layout.leftMargin: Pipboy.Style.margins.large
        Layout.rightMargin: Pipboy.Style.margins.large
        Layout.topMargin: Pipboy.Style.margins.medium

        visible: Pipboy.Messages.unreadCount > 0 && root.messagesVisible && !ApplicationWindow.window.fullscreen

        Connections {
            target: Pipboy.Messages

            function onUnreadCountChanged() {
                if (Pipboy.Messages.unreadCount > 0) {
                    Pipboy.SoundEffects.play("message")
                }
            }
        }

        message: "%1 New Message(s)".arg(Pipboy.Messages.unreadCount)

        Button {
            anchors.right: parent.right
            anchors.rightMargin: Pipboy.Style.margins.small
            anchors.verticalCenter: parent.verticalCenter
            text: "Show"
            flat: true

            onClicked: root.showMessages()
        }
    }

    Item {
        Layout.fillWidth: true
        implicitHeight: Pipboy.Style.margins.large
        visible: (systemNotification.visible || messageNotification.visible) && !keyboard.visible
    }

    VKBD.Keyboard {
        id: keyboard

        Layout.fillWidth: true
        Layout.topMargin: Pipboy.Style.margins.medium
        Layout.preferredHeight: width * 0.3
        Layout.maximumWidth: 850
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

        visible: root.keyboardVisible
    }
}
