package nl.vault25.pipboy;

import org.qtproject.qt5.android.QtNative;
import android.content.Context;
import android.media.AudioManager;

public class MusicVolume {
    private static AudioManager am()
    {
        Context c = QtNative.activity().getApplicationContext();
        return (AudioManager)c.getSystemService(c.AUDIO_SERVICE);
    }

    public static int maximumVolume()
    {
        return am().getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    }

    public static int volume()
    {
        return am().getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    public static void setStreamVolume(int volume)
    {
        am().setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
    }

    public static void incrementVolume()
    {
        am().adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, 0);
    }

    public static void decrementVolume()
    {
        am().adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, 0);
    }
}
