package nl.vault25.pipboy;

import org.qtproject.qt5.android.bindings.QtActivity;

import android.view.WindowManager;
import android.view.Window;
import android.os.Bundle;
import android.media.AudioManager;

public class MainActivity extends QtActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }
}
