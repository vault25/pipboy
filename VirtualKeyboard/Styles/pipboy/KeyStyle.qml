import QtQuick 2.7
import QtQuick.VirtualKeyboard 2.1
import QtQuick.VirtualKeyboard.Styles 2.1

import Pipboy 1.0

Item {
    id: base

    property Item panel;
    property string text: ""
    property bool upperCase: panel ? panel.control.uppercased : false
    property bool pressed: panel ? panel.control.pressed : false

    anchors.fill: panel
    anchors.margins: currentStyle.keyBackgroundMargin;

    Rectangle {
        id: keyBackground

        color: Style.colors.tertiary
        anchors.fill: parent
    }

    Text {
        id: keyText
        text: base.text
        color: Style.colors.primaryText
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.fill: parent
        font: base.upperCase ? Style.fonts.normalUpperCase : Style.fonts.normal
    }

    states: [
        State {
            name: "pressed"
            when: base.pressed
            PropertyChanges {
                target: keyBackground
                color: Style.colors.primary
            }
            PropertyChanges {
                target: keyText
                color: Style.colors.primaryText
            }
        },
        State {
            name: "disabled"
            when: !base.enabled
            PropertyChanges {
                target: keyBackground
                opacity: 0.75
            }
            PropertyChanges {
                target: keyText
                opacity: 0.05
            }
        }
    ]
}
