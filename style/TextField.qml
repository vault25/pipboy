/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Quick Controls 2 module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T

import Pipboy 1.0

T.TextField {
    id: control

    implicitWidth: Math.max(background ? background.implicitWidth : 0,
                            placeholderText ? placeholder.implicitWidth + leftPadding + rightPadding : 0)
                            || contentWidth + leftPadding + rightPadding
    implicitHeight: Math.max(contentHeight + topPadding + bottomPadding,
                             background ? background.implicitHeight : 0,
                             placeholder.implicitHeight + topPadding + bottomPadding)

    padding: Style.margins.medium
    leftPadding: Style.margins.large
    rightPadding: Style.margins.large

    color: Style.colors.primary
    selectionColor: Style.colors.primary
    selectedTextColor: Style.colors.primaryText
    verticalAlignment: TextInput.AlignVCenter

    font: Style.fonts.normal

    cursorDelegate: Rectangle {
        width: metrics.averageCharacterWidth;
        height: metrics.height;

        color: control.color
        visible: control.activeFocus

        SequentialAnimation on opacity {
            running: true
            loops: Animation.Infinite
            PropertyAction { value: 1 }
            PauseAnimation { duration: 200 }
            PropertyAction { value: 0 }
            PauseAnimation { duration: 200 }
        }
    }

    property string displayText
    text: Style.displayText(displayText)

    FontMetrics { id: metrics; font: control.font }

    PlaceholderText {
        id: placeholder
        x: control.leftPadding
        y: control.topPadding
        width: control.width - (control.leftPadding + control.rightPadding)
        height: control.height - (control.topPadding + control.bottomPadding)

        text: Style.displayText(control.placeholderText)
        font: control.font
        opacity: 0.5
        color: Style.colors.primary
        verticalAlignment: control.verticalAlignment
//         visible: !control.length && !control.preeditText && (!control.activeFocus || control.horizontalAlignment !== Qt.AlignHCenter)
        visible: false
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitWidth: 200
        implicitHeight: 40
        color: Style.colors.background

        border.width: Style.sizes.borderWidth
        border.color: control.activeFocus ? Style.colors.primary : Style.colors.secondary
    }
}
