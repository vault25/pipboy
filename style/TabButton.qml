/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Quick Controls 2 module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T

import Pipboy 1.0

T.TabButton {
    id: control

    implicitWidth: Math.max(background ? background.implicitWidth : 0,
                            contentItem.implicitWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(background ? background.implicitHeight : 0,
                             contentItem.implicitHeight + topPadding + bottomPadding)
    baselineOffset: contentItem.y + contentItem.baselineOffset

    padding: 6

    font: Style.fonts.capsBold

    property Item tabBar: parent && parent.parent && parent.parent.tabBar !== undefined ? parent.parent.tabBar : null

    width: tabBar && tabBar.subTabs ? label.width : undefined

    contentItem: Item {
        Label {
            id: label
            anchors.centerIn: parent
            leftPadding: Style.margins.small
            rightPadding: Style.margins.small
            text: Style.displayText(control.text)
            font: control.font
            elide: Text.ElideRight
            opacity: (control.tabBar != null && control.tabBar.subTabs) && !control.checked ? 0.3 : 1.0
            color: Style.colors.primary
            background: Rectangle { color: Style.colors.background }
        }
    }

    background: Item {
        implicitHeight: 40
        visible: !(control.tabBar && control.tabBar.subTabs)

        Rectangle {
            anchors {
                top: parent.top
                bottom: parent.bottom
                topMargin: Style.margins.large
                horizontalCenter: parent.horizontalCenter
            }
            width: label.width + Style.margins.large
            color: Style.colors.primary
            visible: control.checked

            Rectangle {
                anchors.fill: parent
                anchors.margins: Style.sizes.borderWidth
                anchors.bottomMargin: 0
                color: Style.colors.background
            }
        }
    }
}
