/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Quick Controls 2 module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Controls 2.15

import Pipboy 1.0 as Pipboy

T.Dialog {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            contentWidth + leftPadding + rightPadding,
                            implicitHeaderWidth,
                            implicitFooterWidth)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             contentHeight + topPadding + bottomPadding
                             + (implicitHeaderHeight > 0 ? implicitHeaderHeight + spacing : 0)
                             + (implicitFooterHeight > 0 ? implicitFooterHeight + spacing : 0))

    padding: Pipboy.Style.margins.medium

    background: Rectangle {
        color: control.modal ? Pipboy.Style.colors.ocBackground : Pipboy.Style.colors.background
        border.color: control.modal ? Pipboy.Style.colors.ocPrimary : Pipboy.Style.colors.primary
        border.width: Pipboy.Style.sizes.borderWidth
    }

    header: Label {
        text: control.modal ? control.title : Pipboy.Style.displayText(control.title)
        visible: control.title
        elide: Label.ElideRight
        font: Pipboy.Style.fonts.capsBold
        padding: Pipboy.Style.margins.medium
        color: control.modal ? Pipboy.Style.colors.ocPrimary : Pipboy.Style.colors.primary

        background: Item {
            Rectangle {
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                height: Pipboy.Style.sizes.borderWidth
                color: control.modal ? Pipboy.Style.colors.ocPrimary : Pipboy.Style.colors.primary
            }
        }
    }

    footer: DialogButtonBox {
        visible: count > 0

        background: Item {
            Rectangle {
                anchors {
                    left: parent.left
                    right: parent.right
                    top: parent.top
                }
                height: Pipboy.Style.sizes.borderWidth
                color: control.modal ? Pipboy.Style.colors.ocPrimary : Pipboy.Style.colors.primary
            }
        }
    }

    T.Overlay.modal: Rectangle {
        color: Qt.rgba(Pipboy.Style.colors.ocBackground.r,
                       Pipboy.Style.colors.ocBackground.g,
                       Pipboy.Style.colors.ocBackground.b,
                       0.5)
    }

    T.Overlay.modeless: Item { }
}
