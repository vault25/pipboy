#define SCANLINE 1
#define SCANLINE_SLOW 1
// #define SCANLINE_DISTORTION 1
#define TUBE_DISTORTION 1
#define MONOCHROME 1
#define GRAIN 1
#define VHS_STRIPES 1
#define VIGNETTE 1

// Qt Inputs
varying highp vec2 qt_TexCoord0;
uniform mediump float qt_Opacity;
uniform sampler2D source;
// End Qt Inputs

uniform highp float time;
uniform highp vec2 image_size;

#ifdef SCANLINE
uniform mediump float scanline_size;
uniform mediump float scanline_speed;
#endif

#ifdef SCANLINE_SLOW
uniform mediump float slowscan_size;
uniform mediump float slowscan_speed;
#endif

#if defined(SCANLINE) || defined(SCANLINE)
uniform mediump float scanline_opacity;
#endif

#ifdef SCANLINE_DISTORTION
uniform mediump float scanline_distortion_scale;
uniform mediump float scanline_distortion_shift;
uniform mediump float scanline_distortion_center;
#endif

#ifdef TUBE_DISTORTION
uniform mediump float tube_distortion_bend;
uniform mediump float tube_distortion_scale;
#endif

#ifdef MONOCHROME
uniform mediump vec4 monochrome_color;
#endif

#ifdef GRAIN
uniform mediump float grain_opacity;
uniform mediump float grain_saturation;
#endif

#ifdef VIGNETTE
uniform mediump float vignette_scale;
uniform mediump float vignette_softness;
#endif

uniform bool video_distortion;

uniform mediump float stripes_count;
uniform mediump float bars_count;

uniform mediump float pixel_size;

uniform mediump float horizontal_scroll_speed;
uniform mediump float vertical_scroll_speed;

uniform mediump float horizontal_sine;
uniform mediump float vertical_sine;

// Utility functions

// Generate a semi-random number
highp float rand(in mediump vec2 co)
{
    // Lovely magic numbers
    // See http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt = dot(co.xy ,vec2(a, b));
    highp float sn = mod(dt, 3.14158);
    return fract(sin(sn) * c);
}

highp float rand(in mediump float c){
    return rand(vec2(c, 1.0));
}

// Perform overlay blending of two float components.
mediump float overlay(in mediump float s, in mediump float d)
{
    return (d < 0.5) ? 2.0 * s * d : 1.0 - 2.0 * (1.0 - s) * (1.0 - d);
}

// Perform overlay blending between two vec3 components.
mediump vec3 overlay(in mediump vec3 s, in mediump vec3 d)
{
    mediump vec3 c;
    c.x = overlay(s.x, d.x);
    c.y = overlay(s.y, d.y);
    c.z = overlay(s.z, d.z);
    return c;
}

mediump float ramp(in mediump float y, in mediump float start, in mediump float end)
{
    mediump float inside = step(start, y) - step(end, y);
    mediump float fact = (y - start) / (end - start) * inside;
    return (1.0 - fact) * inside;
}

// End utility functions

highp vec3 noise(in mediump vec2 position, in mediump vec2 size, in mediump float time)
{
    highp float r = rand(vec2((2. + time) * position.x, (2. + time) * position.y));
    highp float g = rand(vec2((5. + time) * position.x, (5. + time) * position.y));
    highp float b = rand(vec2((9. + time) * position.x, (9. + time) * position.y));

    return vec3(r, g, b);
}

mediump float scanline(in mediump vec2 position, in mediump vec2 image_size, in mediump float line_size, in mediump float time, in mediump float speed)
{
    return sin(image_size.y * position.y * line_size + time * speed);
}

mediump vec2 crt(mediump vec2 coord, in mediump float bend, in mediump float distortion, in mediump float scale)
{
    coord = (coord - 0.5) * 2. / scale;
    coord *= 0.5;
    coord.x *= 1.0 + pow((abs(coord.y) / bend * distortion), 2.0);
    coord.y *= 1.0 + pow((abs(coord.x) / bend * distortion), 2.0);
    coord  = (coord / 1.0) + 0.5;
    return coord;
}

mediump vec2 scanline_distort(in mediump vec2 position, in mediump float time, in mediump float scanline_scale, in mediump float scanline_center, in mediump float scanline_shift) {
    mediump float scanline_position = time;//(time / 100.0) * 10.0;
    mediump float mix_amount = clamp(1.0 / abs(position.y - scanline_position) * scanline_scale, 0.0, 1.0);
    position.x = mix(position.x, position.x + (position.x - scanline_center) * scanline_shift, mix_amount);
    return position;
}

// vec2 scanline_distort(vec2 position, in float amount, in float size, in float time) {
//     mediump float scan1 = clamp(cos(position.y * size + time), 0.0, 1.0);
//     mediump float scan2 = clamp(cos(position.y * size + time + 4.0) * 10.0, 0.0, 1.0) ;
//     mediump float distortion = scan1 * scan2 * position.x;
//     position.x -= amount * mix(texture2D(source, vec2(position.x, distortion)).r * distortion, distortion, 0.9);
//     return position;
// }

mediump float onOff(in mediump float a, in mediump float b, in mediump float c)
{
    return step(c, sin(time + a*cos(time*b)));
}

mediump vec3 getVideo(mediump vec2 uv)
{
    mediump vec2 look = uv;
    mediump float window = 1./(1.+20.*(look.y-mod(time/4.,1.))*(look.y-mod(time/4.,1.)));
    look.x = look.x + sin(look.y*10. + time)/50.*onOff(4.,4.,.3)*(1.+cos(time*80.))*window;
    mediump float vShift = 0.4*1.0*(sin(time)*sin(time*20.) + (0.5 + 0.1*sin(time*200.)*cos(time)));
    look.y = mod(look.y + vShift, 1.);
    mediump vec3 video = vec3(texture2D(source,look));
    return video;
}

mediump vec2 screenDistort(mediump vec2 uv)
{
    uv -= vec2(.5,.5);
    uv = uv*1.2*(1./1.2+2.*uv.x*uv.x*uv.y*uv.y);
    uv += vec2(.5,.5);
    return uv;
}

mediump float vignette(mediump vec2 uv, mediump float scale, mediump float softness) {
    uv = (uv - 0.5) * 0.98;
    return clamp(pow(cos(uv.x * 3.1415), scale) * pow(cos(uv.y * 3.1415), scale) * softness, 0.0, 1.0);
}

mediump float stripes(mediump vec2 uv)
{
    return ramp(mod(uv.y* stripes_count + time/2.+sin(time + sin(time* 2.)),1.),0.5,0.6);
}

void main(void)
{
    highp vec2 uv = qt_TexCoord0;
    mediump vec2 uv2 = qt_TexCoord0 * 2. - 1.;

    highp vec3 grain = noise(floor(uv * 200.0), image_size, time);
    highp vec3 grau = vec3 (0.5);

    mediump vec2 crt_uv = uv;

#ifdef TUBE_DISTORTION
    crt_uv = crt(crt_uv, 2.0, tube_distortion_bend, tube_distortion_scale);
#endif

#ifdef SCANLINE_DISTORTION
    crt_uv = scanline_distort(crt_uv, time, scanline_distortion_scale, scanline_distortion_center, scanline_distortion_shift);
#endif

    // Discard anything outside the boundaries of the texture.
    if(crt_uv.x < 0.0 || crt_uv.x > 1.0 || crt_uv.y < 0.0 || crt_uv.y > 1.0)
        discard;

//     mediump float value = abs(min(uv.x, uv.y) - 0.5) * 2.0;
//     mediump vec4 color = vec4(value);

    mediump float value = 1.0 - clamp((sqrt(pow(uv.x - 0.5, 2.0) + pow(uv.y - 0.5, 2.0)) * 2.0), 0.0, 1.0);
    mediump vec4 color = vec4(value) * 0.25;

    mediump vec2 image_uv = crt_uv;

    if (pixel_size > 0.0) {
        lowp float size = abs(pixel_size * sin(time));
        image_uv = floor(image_uv * size) / size;
    }

    if (horizontal_scroll_speed != 0.0) {
        image_uv.x += horizontal_scroll_speed * time;
    }

    if (vertical_scroll_speed != 0.0) {
        image_uv.y += vertical_scroll_speed * time;
    }

    if (horizontal_sine != 0.0) {
        image_uv.x += sin((image_uv.y + time) * horizontal_sine) * (0.5 / horizontal_sine);
    }

    if (vertical_sine != 0.0) {
        image_uv.y += sin((image_uv.x + time) * vertical_sine) * (0.5 / vertical_sine);
    }

    if ( video_distortion )
    {
        color += vec4(getVideo(image_uv), 1.0);
    }
    else
    {
        color += texture2D(source, image_uv);
    }
	
//     if(tube_moire)
//     {
//         color*=1.00+tv_dots_blend*.2*sin(crt_uv.x* float(iResolution.x*5.0*tv_dots));
//         color*=1.00+tv_dots_blend*.2*cos(crt_uv.y* float(iResolution.y))*sin(0.5+crt_uv.x* float(iResolution.x));
//     }

#ifdef VHS_STRIPES
    color *= (1.0 + stripes(crt_uv));
    color *= (12. + mod(crt_uv.y * bars_count + time,1.))/13.;
#endif

//     if ( moire )
//         color *= (.45+(rand(crt_uv * .01 * moire_scale)) * opacity_moire);

#ifdef GRAIN
    grain = mix(grau, grain, grain_opacity);
    highp vec3 bw_grain = vec3(grain.r);
    grain = mix(bw_grain, grain, grain_saturation);
    color = vec4(overlay(grain, vec3(color)), 1.0);
#endif

#ifdef VIGNETTE
    color *= vignette(uv, vignette_scale, vignette_softness);
#endif

//     if ( tv_tube_vignette )
//         color*=1.-pow(length(uv2*uv2*uv2*uv2)*1., 6. * 1./tv_tube_vignette_scale);

#ifdef SCANLINE
    mediump vec4 scanline_color = vec4(scanline(crt_uv, image_size, 1.0 / scanline_size, scanline_speed, time));
#else
    mediump vec4 scanline_color = color;
#endif

#ifdef SCANLINE_SLOW
    scanline_color = mix(scanline_color, vec4(scanline(crt_uv, image_size, 1.0 / slowscan_size, slowscan_speed, time)), 0.5);
#endif

    color = mix(color, scanline_color, scanline_opacity);

#ifdef MONOCHROME
    mediump float luma = (color.r * 0.299 + color.g * 0.587 + color.b * 0.114);
    color = vec4(luma, luma, luma, color.a) * monochrome_color;
#endif

    gl_FragColor = color;
}
