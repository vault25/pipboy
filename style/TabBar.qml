/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Quick Controls 2 module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.15
import QtQuick.Templates 2.15 as T

import Pipboy 1.0

T.TabBar {
    id: control

    implicitWidth: Math.max(background ? background.implicitWidth : 0,
                            contentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(background ? background.implicitHeight : 0,
                             contentHeight + topPadding + bottomPadding)

    property bool subTabs: false
    property int subTabPosition: 0

    spacing: 1
    contentHeight: 40

    bottomPadding: subTabs ? 0 : Style.margins.medium

    contentItem: Item {
        ListView {
            //x: control.subTabs ? control.subTabPosition : 0
            width: parent.width

            property Item tabBar: control

            model: control.contentModel
            currentIndex: control.currentIndex

            spacing: control.spacing
            orientation: ListView.Horizontal
            boundsBehavior: Flickable.StopAtBounds
            flickableDirection: Flickable.AutoFlickIfNeeded
            snapMode: ListView.SnapToItem

            highlightMoveDuration: 0
            highlightRangeMode: control.subTabs ? ListView.StrictlyEnforceRange : ListView.ApplyRange
            preferredHighlightBegin: control.subTabs && currentItem ? control.subTabPosition - currentItem.width / 2 : 40
            preferredHighlightEnd: control.subTabs && currentItem ? control.subTabPosition - currentItem.width / 2 : width - 40
        }
    }

    background: Rectangle {
        color: Style.colors.background

        Rectangle {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: Style.sizes.borderWidth + control.bottomPadding
            color: Style.colors.primary

            visible: !control.subTabs

            Rectangle {
                anchors.fill: parent
                anchors.margins: Style.sizes.borderWidth
                anchors.bottomMargin: 0
                color: Style.colors.background
            }
        }
    }
}
