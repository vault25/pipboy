/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Quick Controls 2 module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Shapes 1.15

import Pipboy 1.0 as Pipboy

T.Dial {
    id: control

    implicitWidth: 184
    implicitHeight: 184

    background: Item {
        width: control.availableWidth
        height: control.availableHeight
        /*color: Pipboy.Style.colors.background
        border.width: Pipboy.Style.sizes.borderWidth
        border.color: Pipboy.Style.colors.primary
        radius: width / 2*/

        Shape {
            ShapePath {
                fillColor: "transparent"
                strokeWidth: Pipboy.Style.sizes.borderWidth
                strokeColor: Pipboy.Style.colors.tertiary
                PathAngleArc {
                    centerX: control.background.width / 2
                    centerY: control.background.height / 2

                    radiusX: control.background.width / 2 - 15
                    radiusY: radiusX

                    startAngle: -230
                    sweepAngle: 280
                }

                PathAngleArc {
                    centerX: control.background.width / 2
                    centerY: control.background.height / 2

                    radiusX: control.background.width / 2 - 35
                    radiusY: radiusX

                    startAngle: -230
                    sweepAngle: 280
                }
            }
        }

        PathView {
            model: 11
            delegate: Item {
                Item {
                    anchors.centerIn: parent;
                    width: Pipboy.Style.sizes.borderWidth;
                    height: 23;
                    rotation: parent.PathView.angle
                    Rectangle {
                        anchors.top: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: Pipboy.Style.sizes.borderWidth
                        color: Pipboy.Style.colors.tertiary
                        height: 35
                    }
                }
                Label { anchors.centerIn: parent; text: modelData; }
            }
            path: Path {
                PathAttribute { name: "angle"; value: -140; }
                PathAngleArc {
                    centerX: control.background.width / 2
                    centerY: control.background.height / 2

                    radiusX: control.background.width / 2 + 10
                    radiusY: radiusX

                    startAngle: -230
                    sweepAngle: 308
                }
                PathAttribute { name: "angle"; value: 168 }
            }
        }
    }

    handle: Rectangle {
        x: background.x + background.width / 2 - handle.width / 2
        y: background.y + background.height / 2 - handle.height / 2
        width: 10
        height: background.height * 0.45

        color: Pipboy.Style.colors.background
        border.width: Pipboy.Style.sizes.borderWidth
        border.color: Pipboy.Style.colors.primary

        Rectangle {
            width: Math.sqrt(parent.width * parent.width / 2)
            height: width
            rotation: 45
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.top
            color: Pipboy.Style.colors.background
            border.width: Pipboy.Style.sizes.borderWidth
            border.color: Pipboy.Style.colors.primary
        }

        Rectangle {
            width: parent.width - parent.border.width * 2
            height: width
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            color: Pipboy.Style.colors.background
        }

        Rectangle {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.bottom
            width: parent.width * 2
            height: width
            radius: width / 2
            color: Pipboy.Style.colors.primary
        }

        transform: [
            Translate {
                y: -handle.height / 2
            },
            Rotation {
                angle: control.angle
                origin.x: handle.width / 2
                origin.y: handle.height / 2
            }
        ]
    }
}
