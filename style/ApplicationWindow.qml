/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Quick Controls 2 module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T

import Pipboy 1.0

T.ApplicationWindow {
    id: window

    default property alias data: contentCenter.data
    property bool distort: false

    property bool fullscreen: false

    property int lowPowerTimeout: 1000
    property bool lowPower: false
    onLowPowerChanged: Style.lowPower = lowPower

    property real contentWidth: content.width
    property real contentHeight: content.height

    property string activeEffect: Style.activeEffect

    Item {
        id: content
        anchors.fill: parent
        anchors.leftMargin: Preferences.windowMargins.left
        anchors.rightMargin: Preferences.windowMargins.right
        anchors.topMargin: Preferences.windowMargins.top
        anchors.bottomMargin: Preferences.windowMargins.bottom

        Item {
            id: contentHeader

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: childrenRect.height
        }

        Item {
            id: contentCenter

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: contentHeader.bottom
            anchors.bottom: contentFooter.top
        }

        Item {
            id: contentFooter

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height: childrenRect.height
        }

        layer.enabled: __advancedGL
        layer.wrapMode: ShaderEffectSource.Repeat
        layer.smooth: true
        layer.effect: ShaderEffect {
            id: mainShader;

            blending: false

            property real time: 0.0;
            NumberAnimation on time { from: 0; to: 20; loops: Animation.Infinite; duration: 100000; running: !window.lowPower }
            property size image_size: Qt.size(window.width, window.height);

            property real scanline_size: 2.0;
            property real scanline_speed: 12.0;

            property real slowscan_size: 5.0;
            property real slowscan_speed: -6.0;

            property real scanline_opacity: 0.02;

            property real scanline_distortion_scale: 0.0005;
            property real scanline_distortion_shift: 1.0;
            property real scanline_distortion_center: 0.25;

            property real tube_distortion_bend: 1.01;
            property real tube_distortion_scale: 0.99;

            property color monochrome_color: Style.colors.monochrome;

            property real grain_opacity: window.activeEffect == "noise" ? window.randomNumber(2.5, 7.5) : 0;
            property real grain_saturation: 0.0;

            property real vignette_scale: 1.5;
            property real vignette_softness: 20.0;

            property bool video_distortion: window.distort;

            property real stripes_count: 1.0;
            property real bars_count: 1.0;

            property real pixel_size: window.activeEffect == "pixelize" ? window.randomNumber(250, 500) : 0

            property vector2d scroll_direction: window.activeEffect == "scroll" ? window.randomVector() : Qt.vector2d(0, 0)
            property real scroll_speed: window.activeEffect == "scroll" ? window.randomNumber(0.5, 2.5) : 0
            property real horizontal_scroll_speed: scroll_direction.x * scroll_speed;
            property real vertical_scroll_speed: scroll_direction.y * scroll_speed;

            property vector2d sine_axis: {
                if (window.activeEffect == "sine") {
                    let xActive = false
                    let yActive = false

                    while (!xActive && !yActive) {
                        xActive = Math.random() > 0.5
                        yActive = Math.random() > 0.5
                    }

                    return Qt.vector2d(xActive ? 1.0 : 0.0, yActive ? 1.0 : 0.0)
                }

                return Qt.vector2d(0.0, 0.0)
            }
            property real horizontal_sine: sine_axis.x * window.randomSignedNumber(100, 200)
            property real vertical_sine: sine_axis.y * window.randomSignedNumber(100, 200)

            fragmentShader: Qt.resolvedUrl("ApplicationWindow.frag")

            Timer {
                id: distortionTimer;
                interval: 500 + Math.random() * 1000;
                running: window.distort
                onTriggered: {
                    window.distort = false
                    interval = 500 + Math.random() * 1000;
                }
            }
        }

        MouseArea {
            anchors.fill: parent
            onPressed: {
                window.lowPower = false;
                lowPowerModeTimer.restart()
                mouse.accepted = false
            }

            Timer {
                id: lowPowerModeTimer
                interval: window.lowPowerTimeout
                running: true
                onTriggered: window.lowPower = true
            }
        }
    }

    color: Style.colors.background

    overlay.modal: Rectangle {
        color: Default.overlayModalColor
    }

    overlay.modeless: Rectangle {
        color: Default.overlayDimColor
    }

    onHeaderChanged: {
        if (header) {
            let h = header
            header = null
            contentHeader.children.push(h)
            h.anchors.left = contentHeader.left
            h.anchors.right = contentHeader.right
        }
    }

    onFooterChanged: {
        if (footer) {
            let f = footer
            footer = null
            contentFooter.children.push(f)
            f.anchors.left = contentFooter.left
            f.anchors.right = contentFooter.right
        }
    }

    function randomNumber(min, max) {
        return min + Math.random() * (max - min);
    }

    function randomSignedNumber(min, max) {
        var result = (Math.random() < 0.5 ? -1.0 : 1.0) * randomNumber(min, max);
        return result
    }

    function randomVector() {
        let vector = Qt.vector2d(randomSignedNumber(0.0, 1.0), randomSignedNumber(0.0, 1.0))
        return vector.normalized()
    }
}
