#include "VolumeController.h"

#include <QtMultimedia/QMediaPlayer>
#include <QtMultimedia/QAudio>
#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QAndroidJniObject>
#endif

#include <QDebug>

VolumeController::VolumeController(QObject* parent)
    : QObject(parent)
{
}

QObject* VolumeController::mediaPlayer() const
{
    return m_mediaPlayer;
}

void VolumeController::setMediaPlayer(QObject* newMediaPlayer)
{
    if (!newMediaPlayer) {
        m_mediaPlayer = nullptr;
        Q_EMIT mediaPlayerChanged();
        return;
    }

    auto player = newMediaPlayer->property("mediaObject").value<QMediaPlayer*>();
    if (player == m_mediaPlayer) {
        return;
    }

    m_mediaPlayer = player;
    Q_EMIT mediaPlayerChanged();
    Q_EMIT volumeChanged();
}

int VolumeController::volume() const
{
#ifndef Q_OS_ANDROID
    if (!m_mediaPlayer) {
        return 0;
    }

    return m_mediaPlayer->volume();
#else
    return QAndroidJniObject::callStaticMethod<int>("nl/vault25/pipboy/MusicVolume", "volume");
#endif
}

void VolumeController::setVolume(int newVolume)
{
#ifndef Q_OS_ANDROID
    if (!m_mediaPlayer) {
        return;
    }

    newVolume = std::max(std::min(newVolume, 100), 0);
    m_mediaPlayer->setVolume(newVolume);
    Q_EMIT volumeChanged();
#else
    QAndroidJniObject::callStaticMethod<void>("nl/vault25/pipboy/MusicVolume", "setVolume", "(I)V", newVolume);
    Q_EMIT volumeChanged();
#endif
}

void VolumeController::increaseVolume()
{
#ifndef Q_OS_ANDROID
    setVolume(volume() + 1);
#else
    QAndroidJniObject::callStaticMethod<void>("nl/vault25/pipboy/MusicVolume", "incrementVolume");
    Q_EMIT volumeChanged();
#endif
}

void VolumeController::reduceVolume()
{
#ifndef Q_OS_ANDROID
    setVolume(volume() - 1);
#else
    QAndroidJniObject::callStaticMethod<void>("nl/vault25/pipboy/MusicVolume", "decrementVolume");
    Q_EMIT volumeChanged();
#endif
}

int VolumeController::maximumVolume() const
{
#ifndef Q_OS_ANDROID
    return 100;
#else
    return QAndroidJniObject::callStaticMethod<int>("nl/vault25/pipboy/MusicVolume", "maximumVolume");
#endif
}
