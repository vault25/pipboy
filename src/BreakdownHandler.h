#ifndef BREAKDOWNHANDLER_H
#define BREAKDOWNHANDLER_H

#include <memory>
#include <QObject>
#include <QLoggingCategory>
#include <QtQml/QQmlListProperty>

class BreakdownSolver;
class Breakdown;

class BreakdownHandler : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("DefaultProperty", "breakdowns")

public:
    BreakdownHandler(QObject* parent = nullptr);
    ~BreakdownHandler();

    Q_PROPERTY(Breakdown* activeBreakdown READ activeBreakdown WRITE setActiveBreakdown NOTIFY activeBreakdownChanged)
    Breakdown* activeBreakdown() const;
    void setActiveBreakdown(Breakdown* breakdown);
    Q_SIGNAL void activeBreakdownChanged();

    Q_PROPERTY(QString activeEffect READ activeEffect NOTIFY activeBreakdownChanged)
    QString activeEffect() const;

    Q_PROPERTY(qreal overallChance READ overallChance WRITE setOverallChance NOTIFY overallChanceChanged)
    qreal overallChance() const;
    void setOverallChance(qreal newOverallChance);
    Q_SIGNAL void overallChanceChanged();

    Q_PROPERTY(int minimumInterval READ minimumInterval WRITE setMinimumInterval NOTIFY minimumIntervalChanged)
    int minimumInterval() const;
    void setMinimumInterval(int newMinimumInterval);
    Q_SIGNAL void minimumIntervalChanged();

    Q_PROPERTY(int maximumInterval READ maximumInterval WRITE setMaximumInterval NOTIFY maximumIntervalChanged)
    int maximumInterval() const;
    void setMaximumInterval(int newMaximumInterval);
    Q_SIGNAL void maximumIntervalChanged();

    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)
    bool running() const;
    void setRunning(bool newRunning);
    Q_SIGNAL void runningChanged();

    Q_PROPERTY(QQmlListProperty<Breakdown> breakdowns READ breakdownsProperty CONSTANT)
    QQmlListProperty<Breakdown> breakdownsProperty();

    QStringList subsystems() const;

    Q_INVOKABLE void trigger();

private:
    void update();

    class Private;
    const std::unique_ptr<Private> d;
};

Q_DECLARE_LOGGING_CATEGORY(breakdownHandler);

#endif // BREAKDOWNHANDLER_H
