#include "Constants.h"

#include <QDateTime>
#include <QTimeZone>

#include <QDebug>

#include <Zax/JsonApi/Query.h>
#include <Zax/JsonApi/ApiResult.h>
#include <Zax/JsonApi/FilterRule.h>
#include <Zax/JsonApi/SortRule.h>

class Constants::Private
{
public:
    void updateInGameDate();

    QDateTime inGameDate;

    int inGameYearOffset = 121; // 2139 - 2017
    int inGameMonthOffset = 4;
    int inGameDayOffset = -6;
    int buildYear = 2059;
    QString version = QStringLiteral("5.2.453");
    float crashChance = 1.0;

    std::unique_ptr<Zax::JsonApi::Query> query;

    static std::shared_ptr<Constants> instance;
};

std::shared_ptr<Constants> Constants::Private::instance;

Constants::Constants(QObject *parent)
    : QObject(parent), d(new Private)
{
    d->query = std::make_unique<Zax::JsonApi::Query>();
    d->query->setPath(QStringLiteral("/node/date_mapping"));
    d->query->setRefreshInterval(std::chrono::minutes(1));
    connect(d->query.get(), &Zax::JsonApi::Query::success, this, &Constants::updateInGameDate);

    QVector<Zax::JsonApi::QueryRule*> rules;

    auto statusFilter = new Zax::JsonApi::FilterRule{this};
    statusFilter->setField(QStringLiteral("status"));
    statusFilter->setValue(1);
    rules.append(statusFilter);

    auto sort = new Zax::JsonApi::SortRule{this};
    sort->setField(QStringLiteral("field_out_of_game_date"));
    sort->setDirection(Zax::JsonApi::SortRule::Descending);
    rules.append(sort);

    d->query->setRules(rules);

    d->query->run();
}

Constants::~Constants()
{
}

QDateTime Constants::inGameDate() const
{
    return d->inGameDate;
}

int Constants::buildYear() const
{
    return d->buildYear;
}

QString Constants::version() const
{
    return d->version;
}

float Constants::crashChance() const
{
    return d->crashChance;
}

std::shared_ptr<Constants> Constants::instance()
{
    if(!Private::instance) {
        Private::instance = std::make_shared<Constants>();
    }

    return Private::instance;
}

QVariant Constants::convertInGameDate(const QVariant& input)
{
    QDateTime date;
    if (input.toString() == QStringLiteral("<current>")) {
        date = instance()->inGameDate();
    } else {
        static const std::array<QString, 5> formats = {
            QStringLiteral("yyyy-M-d h:m:s"),
            QStringLiteral("yyyy-M-d h:m"),
            QStringLiteral("d-M-yyyy h:m"),
            QStringLiteral("yyyy-M-d"),
            QStringLiteral("d-M-yyyy")
        };

        for (const auto& format : formats) {
            date = QDateTime::fromString(input.toString(), format);
            if (date.isValid()) {
                date.setTimeZone(QTimeZone::utc());
                break;
            }
        }
    }
    return date;
}

void Constants::updateInGameDate()
{
    auto now = QDateTime::currentDateTimeUtc();

    auto entries = d->query->entries();
    for (auto entry : entries) {
        auto object = entry.toMap();
        auto ocDate = QDateTime::fromString(object.value("field_out_of_game_date").toString().simplified(), QStringLiteral("yyyy-MM-dd"));
        ocDate.setTimeZone(QTimeZone::utc());
        auto icDate = convertInGameDate(object.value("field_in_game_date").toString().simplified()).value<QDateTime>();

        if (ocDate > now) {
            continue;
        }

        auto difference = now.toSecsSinceEpoch() - ocDate.toUTC().toSecsSinceEpoch();
        d->inGameDate = QDateTime::fromSecsSinceEpoch(icDate.toUTC().toSecsSinceEpoch() + difference);
        Q_EMIT inGameDateChanged();
        return;
    }
}
