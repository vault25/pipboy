#ifndef NOTESMODEL_H
#define NOTESMODEL_H

#include <memory>

#include <QAbstractListModel>

class NotesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        FileNameRole = Qt::UserRole + 1,
        FilePathRole
    };

    explicit NotesModel(QObject *parent = nullptr);
    ~NotesModel();

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE QString contents(int index);
    Q_INVOKABLE bool saveContents(int index, const QString& newContents);
    Q_INVOKABLE bool createNote();
    Q_INVOKABLE bool renameNote(int index, const QString& newName);
    Q_INVOKABLE bool deleteNote(int index);

private:
    class Private;
    const std::unique_ptr<Private> d;
};

#endif // NOTESMODEL_H
