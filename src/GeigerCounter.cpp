#include "GeigerCounter.h"

#include <cmath>
#include <random>
#include <chrono>

#include <QDebug>
#include <QTimer>
#include <QSet>

#include <Zax/JsonApi/Query.h>
#include <Zax/JsonApi/ApiResult.h>

#include <Zax/Location/ProximitySensor.h>
#include <Zax/Location/LocationSource.h>
#include <Zax/Location/DeviceLocationSource.h>
#include <Zax/Location/BluetoothSource.h>
#include <Zax/Location/BluetoothScanner.h>

const QString radiationSourcesPath = QStringLiteral("node/radiation_source");
const auto sourceUpdateInterval = std::chrono::minutes(5);

const auto falloffConstant = 50.0;

double falloff(double level, double distance)
{
    return qMax(0.0, level - (distance * falloffConstant));
}

class GeigerCounter::Private
{
public:
    Private(GeigerCounter* qq) : q(qq) { }

    GeigerCounter* q;

    std::unique_ptr<Zax::Location::ProximitySensor> proximitySensor;

    bool enabled = false;

    double currentRadiation = 0.0;
    double totalRadiation = 0.0;

    QString locationInformation;

    QHash<Zax::Location::LocationSource*, qreal> radiationValues;

    std::unique_ptr<QTimer> resetCurrentTimer;
    std::unique_ptr<Zax::JsonApi::Query> sourcesQuery;

    std::default_random_engine randomEngine;
    std::uniform_real_distribution<double> randomDistribution;
};

GeigerCounter::GeigerCounter(QObject* parent)
    : QObject(parent), d(new Private(this))
{
    d->proximitySensor = std::make_unique<Zax::Location::ProximitySensor>();
    connect(d->proximitySensor.get(), &Zax::Location::ProximitySensor::detected, this, &GeigerCounter::onDetected);

    std::random_device device;
    d->randomEngine = std::default_random_engine{device()};
    d->randomDistribution = std::uniform_real_distribution<double>{0.0, 25.0};

    d->resetCurrentTimer = std::make_unique<QTimer>();
    d->resetCurrentTimer->setInterval(std::chrono::seconds(1));
    d->resetCurrentTimer->setSingleShot(false);
    connect(d->resetCurrentTimer.get(), &QTimer::timeout, this, [this]() {
        d->currentRadiation = d->randomDistribution(d->randomEngine);
        Q_EMIT currentRadiationChanged();

        d->totalRadiation += d->currentRadiation;
        Q_EMIT totalRadiationChanged();
    });

    d->sourcesQuery = std::make_unique<Zax::JsonApi::Query>();
    d->sourcesQuery->setPath(radiationSourcesPath);
    d->sourcesQuery->setRefreshInterval(sourceUpdateInterval);
    connect(d->sourcesQuery.get(), &Zax::JsonApi::Query::success, this, &GeigerCounter::onQuerySuccess);
    d->sourcesQuery->run();
}

GeigerCounter::~GeigerCounter()
{
}

bool GeigerCounter::enabled() const
{
    return d->enabled;
}

double GeigerCounter::currentRadiation() const
{
    return d->currentRadiation;
}

double GeigerCounter::totalRadiation() const
{
    return d->totalRadiation;
}

void GeigerCounter::setTotalRadiation(double value)
{
    d->totalRadiation = value;
    Q_EMIT totalRadiationChanged();
}

void GeigerCounter::setEnabled(bool enable)
{
    if(enable == d->enabled) {
        return;
    }

    d->enabled = enable;

    if(d->enabled) {
        auto deviceLocation = new Zax::Location::DeviceLocationSource(this);
        deviceLocation->setMinimumAccuracy(10.0);
        deviceLocation->setUpdateInterval(std::chrono::milliseconds(500));
        connect(deviceLocation, &Zax::Location::DeviceLocationSource::coordinateChanged, this, &GeigerCounter::onOriginChanged);
        Zax::Location::BluetoothScanner::instance()->setEnabled(true);
        d->proximitySensor->setOrigin(deviceLocation);
        d->proximitySensor->setUpdateInterval(std::chrono::milliseconds(500));
        d->resetCurrentTimer->start();
    } else {
        d->proximitySensor->origin()->deleteLater();
        d->proximitySensor->setOrigin(nullptr);
        d->proximitySensor->setUpdateInterval(0);
        Zax::Location::BluetoothScanner::instance()->setEnabled(false);
        d->resetCurrentTimer->stop();
    }

    Q_EMIT enabledChanged();
}

QString GeigerCounter::locationInformation() const
{
    return d->locationInformation;
}

void GeigerCounter::onDetected(Zax::Location::LocationSource* source, qreal distance)
{
    auto value = falloff(d->radiationValues.value(source), distance);
    if (value > 0.0) {
        d->currentRadiation += value;
        d->totalRadiation += value;

        Q_EMIT currentRadiationChanged();
        Q_EMIT totalRadiationChanged();
    }
}

void GeigerCounter::onQuerySuccess()
{
    auto identifiers = d->proximitySensor->locationIdentifiers();
    auto set = QSet<QString>{identifiers.begin(), identifiers.end()};

    const auto entries = d->sourcesQuery->entries();
    for(const auto& entry : entries)
    {
        auto attributes = entry.toMap();

        auto id = attributes.value(QStringLiteral("id")).toString();

        if (set.contains(id)) {
            set.remove(id);
            continue;
        }

        auto type = attributes.value("field_radiation_source_type").toString();
        auto value = attributes.value("field_radiation_value").toInt();

        Zax::Location::LocationSource* location;

        if (type == QStringLiteral("gps")) {
            location = new Zax::Location::LocationSource(this);
            auto coordinate = attributes.value("field_gps_location").toMap();
            location->setIdentifier(id);
            location->setCoordinate(QGeoCoordinate{coordinate.value("lat").toDouble(), coordinate.value("lon").toDouble()});
            location->setTriggerDistance(value / falloffConstant);
        } else if (type == QStringLiteral("bluetooth")) {
            location = new Zax::Location::BluetoothSource{this};
            location->setIdentifier(attributes.value(QStringLiteral("field_bluetooth_device")).toString());
            location->setTriggerDistance(value / falloffConstant);
        } else {
            qDebug() << "Unknown radiation source type" << type;
            continue;
        }

        set.remove(id);
        d->proximitySensor->addLocation(location);
        d->radiationValues.insert(location, value);
    }

    for (auto id : set) {
        d->proximitySensor->removeLocation(id);
    }
}

void GeigerCounter::onOriginChanged()
{
    auto origin = static_cast<Zax::Location::DeviceLocationSource*>(d->proximitySensor->origin());
    auto c = origin->coordinate();

    d->locationInformation = QStringLiteral("Location: Lat %1 Lon %2 Accuracy %3").arg(c.latitude()).arg(c.longitude()).arg(origin->accuracy());

    Q_EMIT locationInformationChanged();
}
