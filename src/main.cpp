
#include <memory>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <QFontDatabase>
#include <QDebug>
#include <QCommandLineParser>

#include <QOpenGLContext>
#include <QSurfaceFormat>
#include <QOpenGLShader>
#include <QOffscreenSurface>

#include <Zax/JsonApi/Api.h>
#include <Zax/JsonApi/ApiModel.h>

#include <Zax/Identity/Identity.h>
#include <Zax/Identity/JsonApiProvider.h>

#include "GalleryModel.h"
#include "NotesModel.h"
#include "Constants.h"
#include "GeigerCounter.h"
#include "VolumeController.h"
#include "MessageWatcher.h"
#include "Breakdown.h"
#include "BreakdownSolver.h"
#include "BreakdownHandler.h"
#include "SystemStatusModel.h"
#include "Messages.h"
#include "PermissionManager.h"

#include "Config.h"

enum class RenderType
{
    Software,
    LimitedGL,
    AdvancedGL
};

RenderType detectGL()
{
    QOpenGLContext context{};
    if(!context.create())
        return RenderType::Software;

    QOffscreenSurface surface;
    surface.create();

    context.makeCurrent(&surface);

    auto result = RenderType::AdvancedGL;
    QOpenGLShader shader{QOpenGLShader::Fragment};
    if(!shader.compileSourceFile(":/style/ApplicationWindow.frag")) {
        result = RenderType::LimitedGL;
    }

    context.doneCurrent();
    surface.destroy();

    return result;
}

int Q_DECL_EXPORT main(int argc, char *argv[])
{
    qputenv("QT_VIRTUALKEYBOARD_STYLE", "pipboy");
    qputenv("QT_VIRTUALKEYBOARD_LAYOUT_PATH", "qrc:/qt-project.org/imports/QtQuick/VirtualKeyboard/Layouts");
    qputenv("QT_IM_MODULE", "qtvirtualkeyboard");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QQuickStyle::setStyle("qrc:/style");

    auto app = std::make_shared<QGuiApplication>(argc, argv);
    app->setApplicationName("PipBoy");
    app->setApplicationVersion(APPLICATION_VERSION);
    app->setOrganizationDomain("nl.vault25");

    QCommandLineParser parser;
    parser.addOption({QStringLiteral("skipintro"), QStringLiteral("Skip the bootup introduction.")});
    parser.addOption({QStringLiteral("page"), QStringLiteral("Start at the specified page."), QStringLiteral("page")});
    parser.addHelpOption();
    parser.process(*app.get());

    auto renderType = detectGL();
    if(renderType == RenderType::Software) {
        qputenv("QT_QUICK_BACKEND", "software");
    }

    auto index = QFontDatabase::addApplicationFont(":/fonts/monofonto.ttf");
    if(index == -1) {
        qDebug() << "Could not load main application font";
    } else {
        app->setFont(QFont{"Monofonto"});
    }

    auto api = Zax::JsonApi::Api::instance();
    api->cache()->setLifeTime(std::chrono::minutes(1));

    api->setServers({
        QStringLiteral("https://pipboy.home.quantumproductions.info/")
    });
    api->setUserName("API");
    api->setPassword(API_PASSWORD);
    api->start();

    Zax::JsonApi::ApiModel::registerFieldType(QStringLiteral("in_game_date"), &Constants::convertInGameDate);

    auto provider = std::make_unique<Zax::Identity::JsonApiProvider>(QStringLiteral("/node/device"), QStringLiteral("device"));
    provider->setNameProperty(QStringLiteral("field_character"));
    Zax::Identity::Identity::self()->setProvider(std::move(provider));

    qmlRegisterType<GalleryModel>("Pipboy", 1, 0, "GalleryModel");
    qmlRegisterType<NotesModel>("Pipboy", 1, 0, "NotesModel");
    qmlRegisterType<GeigerCounter>("Pipboy", 1, 0, "GeigerCounter");
    qmlRegisterSingletonInstance("Pipboy", 1, 0, "Constants", Constants::instance().get());

    qmlRegisterType<VolumeController>("Pipboy", 1, 0, "VolumeController");
    qmlRegisterType<MessageWatcher>("Pipboy", 1, 0, "MessageWatcher");
    qmlRegisterType<SystemStatusModel>("Pipboy", 1, 0, "SystemStatusModel");

    qmlRegisterType<PermissionManager>("Pipboy", 1, 0, "PermissionManager");

    qmlRegisterSingletonType(QUrl("qrc:/qml/Style.qml"), "Pipboy", 1, 0, "Style");
    qmlRegisterSingletonType(QUrl("qrc:/qml/Preferences.qml"), "Pipboy", 1, 0, "Preferences");
    qmlRegisterSingletonType(QUrl("qrc:/qml/SoundEffects.qml"), "Pipboy", 1, 0, "SoundEffects");
    qmlRegisterSingletonType(QUrl("qrc:/qml/Breakdowns.qml"), "Pipboy", 1, 0, "Breakdowns");
    qmlRegisterSingletonType(QUrl("qrc:/qml/RadioPlayer.qml"), "Pipboy", 1, 0, "RadioPlayer");
    qmlRegisterSingletonType(QUrl("qrc:/qml/LocationTracker.qml"), "Pipboy", 1, 0, "LocationTracker");

    qmlRegisterSingletonType<Messages>("Pipboy", 1, 0, "Messages", [](QQmlEngine*, QJSEngine*) {
        return new Messages{};
    });

    qmlRegisterSingletonType(QUrl("qrc:/qml/GeigerCounter.qml"), "Pipboy.GeigerCounter", 1, 0, "Counter");

    qmlRegisterType<Breakdown>("Pipboy.Breakdowns", 1, 0, "Breakdown");
    qmlRegisterType<BreakdownHandler>("Pipboy.Breakdowns", 1, 0, "BreakdownHandler");
    qmlRegisterType<BreakdownSolver>("Pipboy.Breakdowns", 1, 0, "BreakdownSolver");
    qmlRegisterType<ShakeSolver>("Pipboy.Breakdowns", 1, 0, "ShakeSolver");
    qmlRegisterType<RebootSolver>("Pipboy.Breakdowns", 1, 0, "RebootSolver");
    qmlRegisterType<TimeSolver>("Pipboy.Breakdowns", 1, 0, "TimeSolver");

    auto engine = std::make_shared<QQmlApplicationEngine>();
    engine->rootContext()->setContextProperty("__advancedGL", renderType == RenderType::AdvancedGL);
    engine->rootContext()->setContextProperty("__skipIntro", parser.isSet("skipintro"));
    engine->rootContext()->setContextProperty("__page", parser.value("page"));
    engine->load(QUrl(QLatin1String("qrc:/qml/Pipboy.qml")));
    if (engine->rootObjects().isEmpty())
    {
        qDebug() << "Failed to initialize QML!";
        return -1;
    }

    return app->exec();
}
