/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>

#include <QObject>
#include <QDateTime>

class Channel : public QObject
{
    Q_OBJECT

public:
    Channel(QObject* parent = nullptr);

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString id READ id NOTIFY idChanged)
    QString id() const;
    void setId(const QString& newId);
    Q_SIGNAL void idChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    QString name() const;
    void setName(const QString& newName);
    Q_SIGNAL void nameChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(int unreadCount READ unreadCount NOTIFY unreadCountChanged)
    int unreadCount() const;
    void setUnreadCount(int newUnread);
    Q_SIGNAL void unreadCountChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QDateTime lastRead READ lastRead WRITE setLastRead NOTIFY lastReadChanged)
    QDateTime lastRead() const;
    void setLastRead(const QDateTime & newLastRead);
    Q_SIGNAL void lastReadChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(bool postingEnabled READ postingEnabled NOTIFY postingEnabledChanged)
    bool postingEnabled() const;
    void setPostingEnabled(bool newPostingEnabled);
    Q_SIGNAL void postingEnabledChanged();

private:
    QString m_id;
    QString m_name;
    int m_unreadCount = 0;
    QDateTime m_lastRead = QDateTime::currentDateTimeUtc();
    bool m_postingEnabled = false;
};

class Messages : public QObject
{
    Q_OBJECT

public:
    Messages(QObject* parent = nullptr);
    ~Messages() override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QList<Channel*> channels READ channels NOTIFY channelsChanged)
    QList<Channel*> channels() const;
    Q_SIGNAL void channelsChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(int unreadCount READ unreadCount NOTIFY unreadCountChanged)
    int unreadCount() const;
    Q_SIGNAL void unreadCountChanged();

    Q_INVOKABLE void sendMessage(const QString& channelId, const QString& message);
    Q_SIGNAL void messagePosted();
    Q_SIGNAL void messageError(const QString& errorString);

private:
    void onChannelSuccess();
    void onMessageSuccess();

    void onPostSuccess();
    void onPostError();

    class Private;
    const std::unique_ptr<Private> d;
};
