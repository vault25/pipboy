#include "MessageWatcher.h"

#include <QTimer>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <Zax/JsonApi/Query.h>
#include <Zax/JsonApi/ClientFilterRule.h>
#include <Zax/JsonApi/FilterRule.h>
#include <Zax/JsonApi/IncludeRule.h>
#include <Zax/JsonApi/SortRule.h>

#include <Zax/Identity/Identity.h>

class MessageWatcher::Private
{
public:
    QDateTime lastRead;
    int unreadCount;

    std::unique_ptr<Zax::JsonApi::Query> query;
};

MessageWatcher::MessageWatcher(QObject* parent)
    : QObject(parent), d(std::make_unique<Private>())
{
    d->query = std::make_unique<Zax::JsonApi::Query>();
    d->query->setCacheResults(false);
    d->query->setPath(QStringLiteral("/node/message"));
    d->query->setRefreshInterval(10000);

    QVector<Zax::JsonApi::QueryRule*> rules;

    auto statusFilter = new Zax::JsonApi::FilterRule{d->query.get()};
    statusFilter->setField(QStringLiteral("status"));
    statusFilter->setValue(1);
    rules.append(statusFilter);

    auto include = new Zax::JsonApi::IncludeRule{d->query.get()};
    include->setField(QStringLiteral("field_channel"));
    rules.append(include);

    auto sort = new Zax::JsonApi::SortRule{d->query.get()};
    sort->setField(QStringLiteral("changed"));
    sort->setDirection(Zax::JsonApi::SortRule::Descending);
    rules.append(sort);

    auto deviceFilter = new Zax::JsonApi::ClientFilterRule{d->query.get()};
    deviceFilter->setEmpty(Zax::JsonApi::ClientFilterRule::AcceptEmpty);
    deviceFilter->setField(QStringLiteral("field_devices"));
    deviceFilter->setValue(Zax::Identity::Identity::self()->id());
    rules.append(deviceFilter);

    d->query->setRules(rules);

    d->query->run();
}

MessageWatcher::~MessageWatcher()
{
}

int MessageWatcher::unreadCount() const
{
    return d->unreadCount;
}

QDateTime MessageWatcher::lastRead() const
{
    return d->lastRead;
}

void MessageWatcher::setLastRead(const QDateTime& newLastRead)
{
    if (newLastRead == d->lastRead) {
        return;
    }

    d->lastRead = newLastRead.toUTC();
    d->unreadCount = 0;
    Q_EMIT lastReadChanged();
    Q_EMIT unreadCountChanged();
}

void MessageWatcher::update()
{
    auto newUnreadCount = 0;
    const auto entries = d->query->entries();
    for (auto entry : entries) {
        auto changed = entry.toMap().value("changed").toDateTime();
        if (changed > d->lastRead) {
            newUnreadCount++;
        }
    }

    if (newUnreadCount != d->unreadCount) {
        d->unreadCount = newUnreadCount;
        Q_EMIT unreadCountChanged();
    }
}
