#ifndef BREAKDOWN_H
#define BREAKDOWN_H

#include <memory>
#include <QObject>
#include <QtQml/QQmlListProperty>

class BreakdownSolver;

class Breakdown : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("DefaultProperty", "solvers")

public:
    Breakdown(QObject* parent = nullptr);
    ~Breakdown() override;

    Q_PROPERTY(qreal chance READ chance WRITE setChance NOTIFY chanceChanged)
    qreal chance() const;
    void setChance(qreal newChance);
    Q_SIGNAL void chanceChanged();

    Q_PROPERTY(QString subsystem READ subsystem WRITE setSubsystem NOTIFY subsystemChanged)
    QString subsystem() const;
    void setSubsystem(const QString& newSubsystem);
    Q_SIGNAL void subsystemChanged();

    Q_PROPERTY(QString code READ code WRITE setCode NOTIFY codeChanged)
    QString code() const;
    void setCode(const QString& newCode);
    Q_SIGNAL void codeChanged();

    Q_PROPERTY(QString reason READ reason WRITE setReason NOTIFY reasonChanged)
    QString reason() const;
    void setReason(const QString& newReason);
    Q_SIGNAL void reasonChanged();

    Q_PROPERTY(bool critical READ critical WRITE setCritical NOTIFY criticalChanged)
    bool critical() const;
    void setCritical(bool newCritical);
    Q_SIGNAL void criticalChanged();

    Q_PROPERTY(QString activeEffect READ activeEffect CONSTANT)
    QString activeEffect() const;

    Q_PROPERTY(QStringList effects READ effects WRITE setEffects NOTIFY effectsChanged)
    QStringList effects() const;
    void setEffects(const QStringList& newEffects);
    Q_SIGNAL void effectsChanged();

    Q_PROPERTY(BreakdownSolver* activeSolver READ activeSolver CONSTANT)
    BreakdownSolver* activeSolver() const;
    Q_SIGNAL void solved();

    Q_PROPERTY(QQmlListProperty<BreakdownSolver> solvers READ solversProperty CONSTANT)
    QQmlListProperty<BreakdownSolver> solversProperty();

    Breakdown* activeFromCurrent() const;

private:
    class Private;
    const std::unique_ptr<Private> d;
};

#endif // BREAKDOWN_H
