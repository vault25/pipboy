/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "Messages.h"

#include <unordered_map>

#include <QVector>
#include <QDebug>
#include <QJsonObject>

#include <Zax/JsonApi/Query.h>
#include <Zax/JsonApi/FilterRule.h>
#include <Zax/JsonApi/SortRule.h>
#include <Zax/JsonApi/ClientFilterRule.h>
#include <Zax/JsonApi/Api.h>
#include <Zax/JsonApi/ApiResult.h>

#include <Zax/Identity/Identity.h>

#include "Constants.h"

using namespace Zax;

Channel::Channel(QObject* parent)
    : QObject(parent)
{
}

QString Channel::id() const
{
    return m_id;
}

void Channel::setId(const QString & newId)
{
    if (newId == m_id) {
        return;
    }

    m_id = newId;
    Q_EMIT idChanged();
}

QString Channel::name() const
{
    return m_name;
}

void Channel::setName(const QString & newName)
{
    if (newName == m_name) {
        return;
    }

    m_name = newName;
    Q_EMIT nameChanged();
}

int Channel::unreadCount() const
{
    return m_unreadCount;
}

void Channel::setUnreadCount(int newUnreadCount)
{
    if (newUnreadCount == m_unreadCount) {
        return;
    }

    m_unreadCount = newUnreadCount;
    Q_EMIT unreadCountChanged();
}

QDateTime Channel::lastRead() const
{
    return m_lastRead;
}

void Channel::setLastRead(const QDateTime& newLastRead)
{
    if (newLastRead == m_lastRead) {
        return;
    }

    m_lastRead = newLastRead;
    setUnreadCount(0);
    Q_EMIT lastReadChanged();
}

bool Channel::postingEnabled() const
{
    return m_postingEnabled;
}

void Channel::setPostingEnabled(bool newPostingEnabled)
{
    if (newPostingEnabled == m_postingEnabled) {
        return;
    }

    m_postingEnabled = newPostingEnabled;
    Q_EMIT postingEnabledChanged();
}

class Messages::Private
{
public:
    std::unique_ptr<JsonApi::Query> channelQuery;
    std::unique_ptr<JsonApi::Query> messageQuery;

    QList<Channel*> channels;

    std::unordered_map<QString, Channel*> channelMap;

    int unreadCount = 0;

    JsonApi::ApiResultPtr postResult;
};

Messages::Messages(QObject* parent)
    : QObject(parent)
    , d(std::make_unique<Private>())
{
    d->channelQuery = std::make_unique<JsonApi::Query>();
    d->channelQuery->setPath(QStringLiteral("node/channel"));
    d->channelQuery->setRefreshInterval(std::chrono::minutes(1));
    d->channelQuery->setRefreshOnChange(true);
    connect(d->channelQuery.get(), &JsonApi::Query::success, this, &Messages::onChannelSuccess);

    auto channelRules = QVector<JsonApi::QueryRule*>();

    auto filterRule = new JsonApi::FilterRule{d->channelQuery.get()};
    filterRule->setField(QStringLiteral("status"));
    filterRule->setValue(1);
    channelRules.append(filterRule);

    auto sortRule = new JsonApi::SortRule{d->channelQuery.get()};
    sortRule->setFields({QStringLiteral("field_weight"), QStringLiteral("title")});
    channelRules.append(sortRule);

    auto clientFilterRule = new JsonApi::ClientFilterRule{d->channelQuery.get()};
    clientFilterRule->setField(QStringLiteral("field_devices"));
    clientFilterRule->setEmpty(JsonApi::ClientFilterRule::AcceptEmpty);
    clientFilterRule->setValue(Identity::Identity::self()->id());
    channelRules.append(clientFilterRule);

    connect(Identity::Identity::self().get(), &Identity::Identity::changed, clientFilterRule, [clientFilterRule]() {
        clientFilterRule->setValue(Identity::Identity::self()->id());
    });

    d->channelQuery->setRules(channelRules);
    d->channelQuery->run();

    d->messageQuery = std::make_unique<JsonApi::Query>();
    d->messageQuery->setPath(QStringLiteral("node/message"));
    d->messageQuery->setCacheResults(false);
    d->messageQuery->setRefreshInterval(std::chrono::seconds(10));
    connect(d->messageQuery.get(), &JsonApi::Query::success, this, &Messages::onMessageSuccess);

    auto messageRules = QVector<JsonApi::QueryRule*>();

    filterRule = new JsonApi::FilterRule{d->messageQuery.get()};
    filterRule->setField(QStringLiteral("status"));
    filterRule->setValue(1);
    messageRules.append(filterRule);

    sortRule = new JsonApi::SortRule{d->messageQuery.get()};
    sortRule->setField(QStringLiteral("changed"));
    sortRule->setDirection(JsonApi::SortRule::Direction::Descending);
    messageRules.append(sortRule);

    d->messageQuery->setRules(messageRules);
    d->messageQuery->run();
}

Messages::~Messages() = default;

QList<Channel*> Messages::channels() const
{
    return d->channels;
}

int Messages::unreadCount() const
{
    return std::accumulate(d->channels.begin(), d->channels.end(), 0, [](int counter, Channel* channel) {
        return counter + channel->unreadCount();
    });
}

void Messages::sendMessage(const QString& channelId, const QString& message)
{
    if (channelId.isEmpty() || message.isEmpty()) {
        return;
    }

    auto data = QJsonObject{
        {"data", QJsonObject{
            {"type", QStringLiteral("node--message")},
            {"attributes", QJsonObject{
                {"field_in_game_date", Constants::instance()->inGameDate().toUTC().toString(QStringLiteral("yyyy-MM-dd hh:mm:ss"))},
                {"body", message}
            }},
            {"relationships", QJsonObject{
                {"field_device", QJsonObject{
                    {"data", QJsonObject{
                        {"type", "node--device"},
                        {"id", Identity::Identity::self()->id()},
                    }}
                }},
                {"field_channel", QJsonObject{
                    {"data", QJsonObject{
                        {"type", "node--channel"},
                        {"id", channelId}
                    }}
                }}
            }}
        }}
    };

    d->postResult = JsonApi::Api::instance()->post(QStringLiteral("node/message"), data);
    d->postResult->setValidationCallback(JsonApi::Api::validateJsonApi);
    connect(d->postResult.get(), &JsonApi::ApiResult::success, this, &Messages::onPostSuccess);
    connect(d->postResult.get(), &JsonApi::ApiResult::error, this, &Messages::onPostError);
}

void Messages::onChannelSuccess()
{
    auto initialSize = d->channels.size();

    d->channelMap.clear();

    const auto entries = d->channelQuery->entries();
    for (int index = 0; index < entries.size(); ++index) {
        auto entry = entries.at(index).toMap();

        if (d->channels.size() <= index) {
            d->channels.append(new Channel(this));
        }

        auto channel = d->channels.at(index);
        channel->setId(entry.value(QStringLiteral("id")).toString());
        channel->setName(entry.value(QStringLiteral("title")).toString());
        channel->setPostingEnabled(entry.value(QStringLiteral("field_posting_enabled")).toBool());

        connect(channel, &Channel::unreadCountChanged, this, &Messages::unreadCountChanged);

        d->channelMap.insert({channel->id(), channel});
    }

    while (d->channels.size() > entries.size()) {
        d->channels.takeLast()->deleteLater();
    }

    if (initialSize != d->channels.size()) {
        d->messageQuery->run();
        Q_EMIT channelsChanged();
    }
}

void Messages::onMessageSuccess()
{
    const auto entries = d->messageQuery->entries();

    std::unordered_map<Channel*, int> unreadCounts;

    for (auto entry : entries) {
        auto object = entry.toMap();

        auto channelId = object.value("field_channel").toMap().value("id").toString();

        Channel* channel = nullptr;
        try {
            channel = d->channelMap.at(channelId);
        } catch (std::out_of_range&) {
            continue;
        }

        auto changed = QDateTime::fromString(object.value("changed").toString(), Qt::ISODate);

        if (changed > channel->lastRead()) {
            if (unreadCounts.find(channel) == unreadCounts.end()) {
                unreadCounts[channel] = 1;
            } else {
                unreadCounts[channel] += 1;
            }
        }
    }

    for (auto entry : unreadCounts) {
        entry.first->setUnreadCount(entry.second);
    }
}

void Messages::onPostSuccess()
{
    Q_EMIT messagePosted();
    d->postResult.reset();
}

void Messages::onPostError()
{
    Q_EMIT messageError(d->postResult->errorString());
    d->postResult.reset();
}
