#include "Breakdown.h"

#include <random>

#include <QList>
#include <QPointer>

#include "BreakdownSolver.h"

class Breakdown::Private
{
public:
    qreal chance = 0.0;
    QString code;
    QString subsystem;
    QString reason;
    bool critical = false;
    QString activeEffect;
    QStringList effects;
    QPointer<BreakdownSolver> activeSolver;
    QList<BreakdownSolver*> solvers;

    std::mt19937_64 randomGenerator;
};

Breakdown::Breakdown(QObject* parent)
    : QObject(parent)
    , d(std::make_unique<Private>())
{
    std::random_device device;
    d->randomGenerator.seed(device());
}

Breakdown::~Breakdown()
{
    if (d->activeSolver) {
        d->activeSolver->stop();
    }
}

qreal Breakdown::chance() const
{
    return d->chance;
}

void Breakdown::setChance(qreal newChance)
{
    if (newChance == d->chance) {
        return;
    }

    d->chance = newChance;
    Q_EMIT chanceChanged();
}

QString Breakdown::subsystem() const
{
    return d->subsystem;
}

void Breakdown::setSubsystem(const QString & newSubsystem)
{
    if (newSubsystem == d->subsystem) {
        return;
    }

    d->subsystem = newSubsystem;
    Q_EMIT subsystemChanged();
}

QString Breakdown::code() const
{
    return d->code;
}

void Breakdown::setCode(const QString & newCode)
{
    if (newCode == d->code) {
        return;
    }

    d->code = newCode;
    Q_EMIT codeChanged();
}

QString Breakdown::reason() const
{
    return d->reason;
}

void Breakdown::setReason(const QString & newReason)
{
    if (newReason == d->reason) {
        return;
    }

    d->reason = newReason;
    Q_EMIT reasonChanged();
}

bool Breakdown::critical() const
{
    return d->critical;
}

void Breakdown::setCritical(bool newCritical)
{
    if (newCritical == d->critical) {
        return;
    }

    d->critical = newCritical;
    Q_EMIT criticalChanged();
}

QString Breakdown::activeEffect() const
{
    return d->activeEffect;
}

QStringList Breakdown::effects() const
{
    return d->effects;
}

void Breakdown::setEffects(const QStringList & newEffects)
{
    if (newEffects == d->effects) {
        return;
    }

    d->effects = newEffects;
    Q_EMIT effectsChanged();
}

BreakdownSolver* Breakdown::activeSolver() const
{
    return d->activeSolver;
}

QQmlListProperty<BreakdownSolver> Breakdown::solversProperty()
{
    return QQmlListProperty<BreakdownSolver>{this, &d->solvers};
}

Breakdown* Breakdown::activeFromCurrent() const
{
    auto breakdown = new Breakdown{};
    breakdown->d->chance = d->chance;
    breakdown->d->subsystem = d->subsystem;
    breakdown->d->code = d->code;
    breakdown->d->reason = d->reason;
    breakdown->d->critical = d->critical;

    if (d->effects.count() > 0) {
        auto effectsDistribution = std::uniform_int_distribution<int>{0, d->effects.count() - 1};
        breakdown->d->activeEffect = d->effects.at(effectsDistribution(d->randomGenerator));
    }

    if (d->solvers.count() > 0) {
        auto solverChances = std::vector<qreal>{};
        std::transform(d->solvers.cbegin(), d->solvers.cend(), std::back_inserter(solverChances), [](BreakdownSolver* solver) {
            return solver->chance();
        });

        auto solverDistribution = std::discrete_distribution<int>{solverChances.begin(), solverChances.end()};
        breakdown->d->activeSolver = d->solvers.at(solverDistribution(d->randomGenerator));
        connect(breakdown->d->activeSolver, &BreakdownSolver::solved, breakdown, &Breakdown::solved);
        breakdown->d->activeSolver->start();
    }

    return breakdown;
}
