#ifndef SYSTEMSTATUSMODEL_H
#define SYSTEMSTATUSMODEL_H

#include <QAbstractListModel>

class BreakdownHandler;
class GeigerCounter;

class SystemStatusModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(BreakdownHandler* handler READ handler WRITE setHandler NOTIFY handlerChanged)
    Q_PROPERTY(GeigerCounter* geigerCounter READ geigerCounter WRITE setGeigerCounter NOTIFY geigerCounterChanged)

public:
    enum Roles {
        SubsystemRole = Qt::UserRole,
        ReasonRole,
        StatusRole,
        CodeRole,
    };

    enum Status {
        Ok,
        Warning,
        Error
    };
    Q_ENUM(Status)

    SystemStatusModel(QObject* parent = nullptr);
    ~SystemStatusModel() override;

    BreakdownHandler* handler() const;
    void setHandler(BreakdownHandler* newHandler);
    Q_SIGNAL void handlerChanged();

    GeigerCounter* geigerCounter() const;
    void setGeigerCounter(GeigerCounter* newGeigerCounter);
    Q_SIGNAL void geigerCounterChanged();

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex & parent) const override;
    QVariant data(const QModelIndex & index, int role) const override;

private:
    void onBreakdownChanged();

    BreakdownHandler* m_handler = nullptr;
    GeigerCounter* m_geigerCounter = nullptr;
    QStringList m_subsystems;
};

#endif // SYSTEMSTATUSMODEL_H
