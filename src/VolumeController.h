#ifndef VOLUMECONTROLLER_H
#define VOLUMECONTROLLER_H

#include <QObject>

class QMediaPlayer;

/**
 * @todo write docs
 */
class VolumeController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QObject* mediaPlayer READ mediaPlayer WRITE setMediaPlayer NOTIFY mediaPlayerChanged)
    Q_PROPERTY(int volume READ volume WRITE setVolume NOTIFY volumeChanged)
    Q_PROPERTY(int maximumVolume READ maximumVolume CONSTANT)

public:
    VolumeController(QObject* parent = nullptr);

    QObject* mediaPlayer() const;
    void setMediaPlayer(QObject* newMediaPlayer);
    Q_SIGNAL void mediaPlayerChanged();

    int volume() const;
    void setVolume(int newVolume);
    Q_SIGNAL void volumeChanged();
    Q_INVOKABLE void reduceVolume();
    Q_INVOKABLE void increaseVolume();

    int maximumVolume() const;

private:
    QMediaPlayer* m_mediaPlayer = nullptr;
};

#endif // VOLUMECONTROLLER_H
