#include "BreakdownSolver.h"

#include <QDebug>

BreakdownSolver::BreakdownSolver(QObject* parent)
    : QObject(parent)
{
}

BreakdownSolver::~BreakdownSolver()
{
}

QString BreakdownSolver::type() const
{
    return m_type;
}

qreal BreakdownSolver::chance() const
{
    return m_chance;
}

void BreakdownSolver::setChance(qreal newChance)
{
    if (newChance == m_chance) {
        return;
    }

    m_chance = newChance;
    Q_EMIT chanceChanged();
}


void BreakdownSolver::start()
{
}

void BreakdownSolver::stop()
{
}

ShakeSolver::ShakeSolver(QObject* parent)
    : BreakdownSolver(parent)
{
    m_type = QStringLiteral("shake");
}

ShakeSolver::~ShakeSolver()
{
    if (m_sensor) {
        m_sensor->stop();
    }
}

void ShakeSolver::start()
{
    m_sensor = std::make_unique<QAccelerometer>();
    m_sensor->setAccelerationMode(QAccelerometer::User);
    if (!m_sensor->connectToBackend()) {
        QMetaObject::invokeMethod(this, [this]() { Q_EMIT solved(); }, Qt::QueuedConnection);
        return;
    }

    m_lastPeakTime = QDateTime::currentDateTimeUtc();
    connect(m_sensor.get(), &QAccelerometer::readingChanged, this, &ShakeSolver::update);
    m_sensor->start();
}

void ShakeSolver::stop()
{
    m_sensor->stop();
}

qreal ShakeSolver::sensitivity() const
{
    return m_sensitivity;
}

void ShakeSolver::setSensitivity(qreal newSensitivity)
{
    if (newSensitivity == m_sensitivity) {
        return;
    }

    m_sensitivity = newSensitivity;
    Q_EMIT sensitivityChanged();
}

qreal ShakeSolver::cooldownDuration() const
{
    return m_cooldownDuration;
}

void ShakeSolver::setCooldownDuration(qreal newCooldownDuration)
{
    if (newCooldownDuration == m_cooldownDuration) {
        return;
    }

    m_cooldownDuration = newCooldownDuration;
    Q_EMIT cooldownDurationChanged();
}

int ShakeSolver::peakGoal() const
{
    return m_peakGoal;
}

void ShakeSolver::setPeakGoal(int newPeakGoal)
{
    if (newPeakGoal == m_peakGoal) {
        return;
    }

    m_peakGoal = newPeakGoal;
    Q_EMIT peakGoalChanged();
}

ShakeSolver::Axis ShakeSolver::axis() const
{
    return m_axis;
}

void ShakeSolver::setAxis(ShakeSolver::Axis newAxis)
{
    if (newAxis == m_axis) {
        return;
    }

    m_axis = newAxis;
    Q_EMIT axisChanged();
}

void ShakeSolver::update()
{
    auto reading = m_sensor->reading();

    bool match = false;
    switch (m_axis) {
        case AllAxis:
            match = (reading->x() > m_sensitivity || reading->y() > m_sensitivity || reading->z() > m_sensitivity);
            break;
        case XAxis:
            match = reading->x() > m_sensitivity;
            break;
        case YAxis:
            match = reading->y() > m_sensitivity;
            break;
        case ZAxis:
            match = reading->z() > m_sensitivity;
            break;
    }

    if (match) {
        qDebug() << "match";
        auto now = QDateTime::currentDateTimeUtc();

        if (now.toMSecsSinceEpoch() - m_lastPeakTime.toMSecsSinceEpoch() < m_cooldownDuration) {
            m_peakCount++;
        } else {
            m_peakCount = 1;
        }

        m_lastPeakTime = now;

        if (m_peakCount >= m_peakGoal) {
            Q_EMIT solved();
            stop();
        }
    }
}

RebootSolver::RebootSolver(QObject* parent)
    : BreakdownSolver(parent)
{
    m_type = QStringLiteral("reboot");
}

void RebootSolver::start()
{
}

void RebootSolver::stop()
{
}

void RebootSolver::rebooted()
{
    Q_EMIT solved();
}

TimeSolver::TimeSolver(QObject* parent)
    : BreakdownSolver(parent)
{
    m_type = QStringLiteral("time");
    std::random_device device;
    m_randomGenerator.seed(device());
}

void TimeSolver::start()
{
    m_timer = std::make_unique<QTimer>();
    m_timer->setInterval(m_randomDistribution(m_randomGenerator));
    m_timer->setSingleShot(true);
    connect(m_timer.get(), &QTimer::timeout, this, &TimeSolver::solved);
    m_timer->start();
}

void TimeSolver::stop()
{
    if (m_timer) {
        m_timer->stop();
    }
}

int TimeSolver::minimumDuration() const
{
    return m_minimumDuration;
}

void TimeSolver::setMinimumDuration(int newMinimumDuration)
{
    if (newMinimumDuration == m_minimumDuration) {
        return;
    }

    m_minimumDuration = newMinimumDuration;
    m_randomDistribution = std::uniform_int_distribution<int>(m_minimumDuration, m_maximumDuration);
    Q_EMIT minimumDurationChanged();
}

int TimeSolver::maximumDuration() const
{
    return m_maximumDuration;
}

void TimeSolver::setMaximumDuration(int newMaximumDuration)
{
    if (newMaximumDuration == m_maximumDuration) {
        return;
    }

    m_maximumDuration = newMaximumDuration;
    m_randomDistribution = std::uniform_int_distribution<int>(m_minimumDuration, m_maximumDuration);
    Q_EMIT maximumDurationChanged();
}

