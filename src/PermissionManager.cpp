/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "PermissionManager.h"

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QtAndroid>
#endif

PermissionManager::PermissionManager(QObject* parent)
    : QObject(parent)
{
}

bool PermissionManager::granted() const
{
    return m_granted;
}

void PermissionManager::request()
{
#ifdef Q_OS_ANDROID
    const auto permissions = QStringList{
        QStringLiteral("android.permission.BLUETOOTH"),
        QStringLiteral("android.permission.BLUETOOTH_ADMIN"),
        QStringLiteral("android.permission.ACCESS_FINE_LOCATION"),
        QStringLiteral("android.permission.READ_EXTERNAL_STORAGE"),
        QStringLiteral("android.permission.WRITE_EXTERNAL_STORAGE")
    };

    QtAndroid::requestPermissions(permissions, [this, permissions](const QtAndroid::PermissionResultMap& result) {
        auto count = std::accumulate(permissions.begin(), permissions.end(), 0, [result](int counter, const QString& permission) -> int {
            return counter + (result[permission] == QtAndroid::PermissionResult::Granted ? 1 : 0);
        });

        m_granted = count == permissions.size();
        Q_EMIT grantedChanged();
    });
#else
    m_granted = true;
    Q_EMIT grantedChanged();
#endif
}
