#include "SystemStatusModel.h"

#include <QDebug>

#include <Zax/JsonApi/Api.h>

#include "BreakdownHandler.h"
#include "Breakdown.h"
#include "GeigerCounter.h"

SystemStatusModel::SystemStatusModel(QObject* parent)
    : QAbstractListModel(parent)
{
    connect(Zax::JsonApi::Api::instance().get(), &Zax::JsonApi::Api::isConnectedChanged, this, [this]() {
        auto row = m_subsystems.indexOf(QStringLiteral("Comms"));
        Q_EMIT dataChanged(index(row, 0), index(row, 0), {StatusRole, ReasonRole, CodeRole});
    });
}

SystemStatusModel::~SystemStatusModel()
{
}

BreakdownHandler* SystemStatusModel::handler() const
{
    return m_handler;
}

void SystemStatusModel::setHandler(BreakdownHandler* newHandler)
{
    if (newHandler == m_handler) {
        return;
    }

    if (m_handler) {
        m_handler->disconnect(this);
    }

    beginResetModel();
    m_handler = newHandler;
    if (m_handler) {
        m_subsystems = m_handler->subsystems();
        connect(m_handler, &BreakdownHandler::activeBreakdownChanged, this, &SystemStatusModel::onBreakdownChanged);
    } else {
        m_subsystems.clear();
    }
    endResetModel();

    Q_EMIT handlerChanged();
}

GeigerCounter* SystemStatusModel::geigerCounter() const
{
    return m_geigerCounter;
}

void SystemStatusModel::setGeigerCounter(GeigerCounter* newGeigerCounter)
{
    if (newGeigerCounter == m_geigerCounter) {
        return;
    }

    if (m_geigerCounter) {
        m_geigerCounter->disconnect(this);
    }

    m_geigerCounter = newGeigerCounter;
    if (m_geigerCounter) {
        connect(m_geigerCounter, &GeigerCounter::enabledChanged, this, [this]() {
            auto row = m_subsystems.indexOf(QStringLiteral("Radiation"));
            Q_EMIT dataChanged(index(row, 0), index(row, 0), { StatusRole, ReasonRole, CodeRole });
        });
    }
    Q_EMIT geigerCounterChanged();
}


QHash<int, QByteArray> SystemStatusModel::roleNames() const
{
    static QHash<int, QByteArray> roleNames = {
        {SubsystemRole, "subsystem"},
        {ReasonRole, "reason"},
        {StatusRole, "status"},
        {CodeRole, "code"}
    };
    return roleNames;
}

int SystemStatusModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid() || !m_handler) {
        return 0;
    }

    return m_subsystems.count();
}

QVariant SystemStatusModel::data(const QModelIndex& index, int role) const
{
    if (!checkIndex(index, CheckIndexOption::DoNotUseParent | CheckIndexOption::DoNotUseParent) || !m_handler) {
        return QVariant{};
    }

    auto subsystem = m_subsystems.at(index.row());
    if (role == SubsystemRole) {
        return subsystem;
    }

    auto breakdown = m_handler->activeBreakdown();

    auto status = Ok;
    auto reason = QString{};
    auto code = QString{};

    if (subsystem == QStringLiteral("Comms") && !Zax::JsonApi::Api::instance()->isConnected()) {
        status = Warning;
        reason = QStringLiteral("Primary network connection failed. Establishing mesh network backup failed. Network resources not available.");
        code = QStringLiteral("0x49C23531");
    } else if (subsystem == QStringLiteral("Radiation") && (!m_geigerCounter || !m_geigerCounter->enabled())) {
        status = Warning;
        reason = QStringLiteral("Could not detect radiation measurement hardware. Radiation readout not available.");
        code = QStringLiteral("0x9AB3A80A");
    } else if (breakdown && breakdown->subsystem() == subsystem) {
        status = Error;
        reason = breakdown->reason();
        code = breakdown->code();
    }

    switch(role) {
        case StatusRole:
            return status;
        case ReasonRole:
            return reason;
        case CodeRole:
            return code;
    }

    return QVariant{};
}

void SystemStatusModel::onBreakdownChanged()
{
    Q_EMIT dataChanged(index(0, 0), index(m_subsystems.count() - 1, 0), {SubsystemRole, StatusRole, ReasonRole});
}
