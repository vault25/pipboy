#pragma once

#include <memory>

#include <QObject>

class Constants : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDateTime inGameDate READ inGameDate NOTIFY inGameDateChanged)
    Q_PROPERTY(int buildYear READ buildYear CONSTANT)
    Q_PROPERTY(QString version READ version CONSTANT)
    Q_PROPERTY(float crashChance READ crashChance CONSTANT)

public:
    explicit Constants(QObject *parent = nullptr);
    ~Constants();

    QDateTime inGameDate() const;
    Q_SIGNAL void inGameDateChanged();

    int buildYear() const;
    QString version() const;
    float crashChance() const;

    static std::shared_ptr<Constants> instance();

    static QVariant convertInGameDate(const QVariant& input);

private:
    void updateInGameDate();

    class Private;
    const std::unique_ptr<Private> d;
};
