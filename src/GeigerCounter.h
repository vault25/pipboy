#ifndef GEIGERCOUNTER_H
#define GEIGERCOUNTER_H

#include <memory>

#include <QObject>

class QBluetoothDeviceInfo;
class QGeoPositionInfo;

namespace Zax {
    namespace Location {
        class LocationSource;
    }
}

/**
 * @todo write docs
 */
class GeigerCounter : public QObject
{
    Q_OBJECT


public:
    /**
     * Constructor
     *
     * @param parent TODO
     */
    GeigerCounter(QObject* parent = nullptr);

    /**
     * Destructor
     */
    ~GeigerCounter();

    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    bool enabled() const;
    void setEnabled(bool enable);
    Q_SIGNAL void enabledChanged();

    Q_PROPERTY(double currentRadiation READ currentRadiation NOTIFY currentRadiationChanged)
    double currentRadiation() const;
    Q_SIGNAL void currentRadiationChanged();

    Q_PROPERTY(double totalRadiation READ totalRadiation NOTIFY totalRadiationChanged)
    double totalRadiation() const;
    Q_SIGNAL void totalRadiationChanged();

    Q_PROPERTY(QString locationInformation READ locationInformation NOTIFY locationInformationChanged)
    QString locationInformation() const;
    Q_SIGNAL void locationInformationChanged();

    Q_INVOKABLE void setTotalRadiation(double value);

private:
    void onDetected(Zax::Location::LocationSource* source, qreal distance);
    void onQuerySuccess();
    void onOriginChanged();

    class Private;
    const std::unique_ptr<Private> d;
};

#endif // GEIGERCOUNTER_H
