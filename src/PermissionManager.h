/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QObject>

class PermissionManager : public QObject
{
    Q_OBJECT

public:
    PermissionManager(QObject* parent = nullptr);

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(bool granted READ granted NOTIFY grantedChanged)
    bool granted() const;
    Q_SIGNAL void grantedChanged();

    Q_INVOKABLE void request();

private:
    bool m_granted = false;
};
