#include "GalleryModel.h"

#include <QStandardPaths>
#include <QUrl>
#include <QDir>
#include <QStringList>

#include <QDebug>

class GalleryModel::Private
{
    public:
        void update();
        QStringList locatePictures(const QString& dir);

        QHash<int, QByteArray> roleNames;
        QStringList images;
};

GalleryModel::GalleryModel(QObject *parent)
    : QAbstractListModel(parent), d(new Private)
{
    d->roleNames.insert(Roles::FileNameRole, "fileName");
    d->roleNames.insert(Roles::FilePathRole, "filePath");

    d->update();
}

GalleryModel::~GalleryModel()
{
}

QHash<int, QByteArray> GalleryModel::roleNames() const
{
    return d->roleNames;
}

int GalleryModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return d->images.size();
}

QVariant GalleryModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto url = QUrl::fromLocalFile(d->images.at(index.row()));
    switch(static_cast<Roles>(role)) {
        case Roles::FileNameRole:
            return url.fileName();
        case Roles::FilePathRole:
            return url;
        default:
            return QVariant();
    }
}

QString GalleryModel::storagePath() const
{
    auto location = QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
    return location + QStringLiteral("/Fallout");
}

void GalleryModel::Private::update()
{
    for(auto location : QStandardPaths::standardLocations(QStandardPaths::PicturesLocation))
    {
        auto dir = QDir{location};
        if(!dir.exists("Fallout"))
            continue;

        images.append(locatePictures(dir.absoluteFilePath("Fallout")));
        images.sort();
    }
}

QStringList GalleryModel::Private::locatePictures(const QString& dir)
{
    auto result = QStringList{};
    auto directory = QDir{dir};
    for(auto entry : directory.entryInfoList(QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs))
    {
        if(entry.isDir())
        {
            result.append(locatePictures(entry.absoluteFilePath()));
        }
        else
        {
            result.append(entry.absoluteFilePath());
        }
    }
    return result;
}
