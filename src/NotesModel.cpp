#include "NotesModel.h"

#include <QStandardPaths>
#include <QUrl>
#include <QDir>
#include <QStringList>
#include <QFile>
#include <QFileInfo>

#include <QDebug>

class NotesModel::Private
{
public:
    Private(NotesModel* qq) : q(qq) { }

    void update();
    QStringList locateNotes(const QString& dir);

    NotesModel* q;

    QHash<int, QByteArray> roleNames;
    QStringList notes;
};

NotesModel::NotesModel(QObject *parent)
    : QAbstractListModel(parent), d(new Private(this))
{
    d->roleNames.insert(Roles::FileNameRole, "fileName");
    d->roleNames.insert(Roles::FilePathRole, "filePath");

    d->update();
}

NotesModel::~NotesModel()
{
}

QHash<int, QByteArray> NotesModel::roleNames() const
{
    return d->roleNames;
}

int NotesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return d->notes.size();
}

QVariant NotesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto url = QUrl::fromLocalFile(d->notes.at(index.row()));
    switch(static_cast<Roles>(role)) {
        case Roles::FileNameRole:
            return url.fileName();
        case Roles::FilePathRole:
            return url;
        default:
            return QVariant();
    }
}

QString NotesModel::contents(int index)
{
    if(index < 0 || index >= d->notes.size()) {
        return QString{};
    }

    QFile file{d->notes.at(index)};
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        auto contents = QString::fromLocal8Bit(file.readAll());
        file.close();
        return contents;
    }

    return QString{};
}

bool NotesModel::saveContents(int index, const QString& newContents)
{
    if(index < 0 || index >= d->notes.size()) {
        return false;
    }

    auto oldContents = contents(index);
    if(oldContents == newContents) {
        return false;
    }

    QFile file{d->notes.at(index)};
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return false;
    }

    file.write(newContents.toLocal8Bit());
    file.close();
    return true;
}

bool NotesModel::createNote()
{
    auto path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    auto dir = QDir{path};
    if(!dir.exists("Fallout")) {
        dir.mkpath("Fallout");
    }
    dir.cd("Fallout");

    auto index = 1;
    auto fileName = QString{"Note 1"};
    while(dir.exists(fileName)) {
        index += 1;
        fileName = QString{"Note %1"}.arg(index);
    }

    QFile file{dir.absoluteFilePath(fileName)};
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    file.write(fileName.toLocal8Bit());
    file.close();

    d->update();
    return true;
}

bool NotesModel::renameNote(int index, const QString& newName)
{
    if(index < 0 || index >= d->notes.size()) {
        return false;
    }

    auto info = QFileInfo{d->notes.at(index)};
    if(info.baseName() == newName)
        return false;

    auto name = newName;
    auto number = 0;
    while(info.dir().exists(newName)) {
        name = QString{"%1 %2"}.arg(newName, number);
    }

    auto result = QFile::rename(info.absoluteFilePath(), info.dir().absoluteFilePath(name));
    d->update();

    return result;
}

bool NotesModel::deleteNote(int index)
{
    if(index < 0 || index >= d->notes.size()) {
        return false;
    }

    auto result = QFile::remove(d->notes.at(index));
    d->update();
    return result;
}

void NotesModel::Private::update()
{
    q->beginResetModel();
    notes.clear();

    for(auto location : QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation)) {
        auto dir = QDir{location};
        if(!dir.exists("Fallout")) {
            continue;
        }

        notes.append(locateNotes(dir.absoluteFilePath("Fallout")));
    }
    notes.sort(Qt::CaseInsensitive);
    q->endResetModel();
}

QStringList NotesModel::Private::locateNotes(const QString& dir)
{
    auto result = QStringList{};
    auto directory = QDir{dir};
    for(auto entry : directory.entryInfoList(QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs))
    {
        if(entry.isDir())
        {
            result.append(locateNotes(entry.absoluteFilePath()));
        }
        else
        {
            result.append(entry.absoluteFilePath());
        }
    }
    return result;
}
