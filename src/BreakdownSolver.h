#ifndef BREAKDOWNSOLVER_H
#define BREAKDOWNSOLVER_H

#include <memory>
#include <random>
#include <QObject>
#include <QTimer>
#include <QDateTime>
#include <QtSensors/QAccelerometer>

class QAccelerometer;

class BreakdownSolver : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString type READ type CONSTANT)
    Q_PROPERTY(qreal chance READ chance WRITE setChance NOTIFY chanceChanged)

public:
    BreakdownSolver(QObject* parent = nullptr);
    ~BreakdownSolver() override;

    QString type() const;

    qreal chance() const;
    void setChance(qreal newChance);
    Q_SIGNAL void chanceChanged();

    virtual void start();
    virtual void stop();

    Q_SIGNAL void solved();

protected:
    QString m_type;

private:
    qreal m_chance = 0.0;
};

class ShakeSolver : public BreakdownSolver
{
    Q_OBJECT
    Q_PROPERTY(qreal sensitivity READ sensitivity WRITE setSensitivity NOTIFY sensitivityChanged)
    Q_PROPERTY(qreal cooldownDuration READ cooldownDuration WRITE setCooldownDuration NOTIFY cooldownDurationChanged)
    Q_PROPERTY(int peakGoal READ peakGoal WRITE setPeakGoal NOTIFY peakGoalChanged)
    Q_PROPERTY(Axis axis READ axis WRITE setAxis NOTIFY axisChanged)

public:
    enum Axis {
        AllAxis,
        XAxis,
        YAxis,
        ZAxis
    };
    Q_ENUM(Axis)

    ShakeSolver(QObject* parent = nullptr);
    ~ShakeSolver() override;

    void start() override;
    void stop() override;

    qreal sensitivity() const;
    void setSensitivity(qreal newSensitivity);
    Q_SIGNAL void sensitivityChanged();

    qreal cooldownDuration() const;
    void setCooldownDuration(qreal newCooldownDuration);
    Q_SIGNAL void cooldownDurationChanged();

    int peakGoal() const;
    void setPeakGoal(int newPeakGoal);
    Q_SIGNAL void peakGoalChanged();

    Axis axis() const;
    void setAxis(Axis newAxis);
    Q_SIGNAL void axisChanged();

private:
    void update();

    std::unique_ptr<QAccelerometer> m_sensor = nullptr;
    qreal m_sensitivity = 0.0;
    qreal m_cooldownDuration = 500.0;
    int m_peakCount = 0;
    int m_peakGoal = 0;
    Axis m_axis = AllAxis;
    QDateTime m_lastPeakTime;
};

class RebootSolver : public BreakdownSolver
{
    Q_OBJECT
public:
    RebootSolver(QObject* parent = nullptr);
    void start() override;
    void stop() override;
    Q_INVOKABLE void rebooted();
};

class TimeSolver : public BreakdownSolver
{
    Q_OBJECT
    Q_PROPERTY(int minimumDuration READ minimumDuration WRITE setMinimumDuration NOTIFY minimumDurationChanged)
    Q_PROPERTY(int maximumDuration READ maximumDuration WRITE setMaximumDuration NOTIFY maximumDurationChanged)

public:
    TimeSolver(QObject* parent = nullptr);

    void start() override;
    void stop() override;

    int minimumDuration() const;
    void setMinimumDuration(int newMinimumDuration);
    Q_SIGNAL void minimumDurationChanged();

    int maximumDuration() const;
    void setMaximumDuration(int newMaximumDuration);
    Q_SIGNAL void maximumDurationChanged();

private:
    int m_minimumDuration = 0;
    int m_maximumDuration = 1000;
    std::unique_ptr<QTimer> m_timer;
    std::mt19937_64 m_randomGenerator;
    std::uniform_int_distribution<int> m_randomDistribution;
};

#endif // BREAKDOWNSOLVER_H
