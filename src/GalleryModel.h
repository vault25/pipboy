#ifndef GALLERYMODEL_H
#define GALLERYMODEL_H

#include <memory>

#include <QAbstractListModel>

class GalleryModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString storagePath READ storagePath CONSTANT)

public:
    enum Roles {
        FileNameRole = Qt::UserRole + 1,
        FilePathRole
    };

    explicit GalleryModel(QObject *parent = nullptr);
    ~GalleryModel();

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QString storagePath() const;

private:
    class Private;
    const std::unique_ptr<Private> d;
};

#endif // GALLERYMODEL_H
