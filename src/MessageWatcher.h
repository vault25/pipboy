#ifndef MESSAGEWATCHER_H
#define MESSAGEWATCHER_H

#include <memory>
#include <QObject>
#include <QDateTime>

class MessageWatcher : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int unreadCount READ unreadCount NOTIFY unreadCountChanged)
    Q_PROPERTY(QDateTime lastRead READ lastRead WRITE setLastRead NOTIFY lastReadChanged)

public:
    MessageWatcher(QObject* parent = nullptr);
    ~MessageWatcher();

    int unreadCount() const;
    Q_SIGNAL void unreadCountChanged();

    QDateTime lastRead() const;
    void setLastRead(const QDateTime & newLastRead);
    Q_SIGNAL void lastReadChanged();

private:
    void update();

    class Private;
    const std::unique_ptr<Private> d;
};

#endif // MESSAGEWATCHER_H
