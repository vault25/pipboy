#include "BreakdownHandler.h"

#include <random>

#include <QTimer>
#include <QDebug>

#include "Breakdown.h"
#include "BreakdownSolver.h"

Q_LOGGING_CATEGORY(breakdownHandler, "nl.vault25.pipboy.breakdown")

class BreakdownHandler::Private
{
public:
    void resetTimer();

    std::unique_ptr<QTimer> timer;
    qreal overallChance = 0.0;
    int minimumInterval = 0;
    int maximumInterval = 60000;

    std::mt19937_64 randomGenerator;
    std::uniform_real_distribution<float> randomDistribution;
    std::uniform_int_distribution<int> timerDistribution;

    Breakdown* activeBreakdown = nullptr;
    QList<Breakdown*> breakdowns;
};

BreakdownHandler::BreakdownHandler(QObject* parent)
    : QObject(parent), d(std::make_unique<Private>())
{
    d->timer = std::make_unique<QTimer>();
    connect(d->timer.get(), &QTimer::timeout, this, &BreakdownHandler::update);
    d->timer->setInterval(1000);
    d->timer->setSingleShot(true);

    std::random_device device;
    d->randomGenerator.seed(device());
}

BreakdownHandler::~BreakdownHandler()
{
}

Breakdown* BreakdownHandler::activeBreakdown() const
{
    return d->activeBreakdown;
}

QString BreakdownHandler::activeEffect() const
{
    if (!d->activeBreakdown) {
        return QString{};
    }

    return d->activeBreakdown->activeEffect();
}

qreal BreakdownHandler::overallChance() const
{
    return d->overallChance;
}

void BreakdownHandler::setOverallChance(qreal newOverallChance)
{
    if (newOverallChance == d->overallChance) {
        return;
    }

    d->overallChance = newOverallChance;
    Q_EMIT overallChanceChanged();
}

int BreakdownHandler::minimumInterval() const
{
    return d->minimumInterval;
}

void BreakdownHandler::setMinimumInterval(int newMinimumInterval)
{
    if (newMinimumInterval == d->minimumInterval) {
        return;
    }

    d->minimumInterval = newMinimumInterval;
    d->timerDistribution = std::uniform_int_distribution<int>(d->minimumInterval, d->maximumInterval);
    d->resetTimer();
    Q_EMIT minimumIntervalChanged();
}

int BreakdownHandler::maximumInterval() const
{
    return d->maximumInterval;
}

void BreakdownHandler::setMaximumInterval(int newMaximumInterval)
{
    if (newMaximumInterval == d->maximumInterval) {
        return;
    }

    d->maximumInterval = newMaximumInterval;
    d->timerDistribution = std::uniform_int_distribution<int>(d->minimumInterval, d->maximumInterval);
    d->resetTimer();
    Q_EMIT maximumIntervalChanged();
}



bool BreakdownHandler::running() const
{
    return d->timer->isActive();
}

void BreakdownHandler::setRunning(bool newRunning)
{
    if (newRunning == d->timer->isActive()) {
        return;
    }

    if (newRunning) {
        qCDebug(breakdownHandler) << "BreakdownHandler active";
        d->resetTimer();
        d->timer->start();
    } else {
        qCDebug(breakdownHandler) << "BreakdownHandler inactive";
        d->timer->stop();
    }
    Q_EMIT runningChanged();
}


QQmlListProperty<Breakdown> BreakdownHandler::breakdownsProperty()
{
    return QQmlListProperty<Breakdown>{this, &d->breakdowns};
}

QStringList BreakdownHandler::subsystems() const
{
    static QStringList subsystems = {
        QStringLiteral("System"),
        QStringLiteral("Display"),
        QStringLiteral("Input"),
        QStringLiteral("Storage"),
        QStringLiteral("Comms"),
        QStringLiteral("Radiation")
    };
    return subsystems;
}

void BreakdownHandler::setActiveBreakdown(Breakdown* breakdown)
{
    if (d->activeBreakdown == breakdown) {
        return;
    }

    if (d->activeBreakdown) {
        d->activeBreakdown->disconnect(this);
        d->activeBreakdown->deleteLater();
    }

    if (breakdown) {
        qCDebug(breakdownHandler) << "Setting active breakdown to" << breakdown->subsystem() << breakdown->code();
        qCDebug(breakdownHandler) << "Active solver:" << breakdown->activeSolver()->type();
        qCDebug(breakdownHandler) << "Active effect:" << breakdown->activeEffect();
    } else if (d->activeBreakdown) {
        qCDebug(breakdownHandler) << "Clearing active breakdown";
    }

    d->activeBreakdown = breakdown;
    if (breakdown) {
        breakdown->setParent(this);
        connect(breakdown, &Breakdown::solved, this, [this]() {
            qCDebug(breakdownHandler) << "Active breakdown solved!";
            setActiveBreakdown(nullptr);
        });
    }

    Q_EMIT activeBreakdownChanged();
}

void BreakdownHandler::update()
{
    d->timer->setInterval(d->timerDistribution(d->randomGenerator));
    d->timer->start();

    if (d->activeBreakdown) {
        return;
    }

    auto random = d->randomDistribution(d->randomGenerator);
    if (random > d->overallChance) {
        return;
    }

    trigger();
}

void BreakdownHandler::trigger()
{
    if (d->activeBreakdown) {
        return;
    }

    auto breakdownChances = std::vector<qreal>{};
    std::transform(d->breakdowns.cbegin(), d->breakdowns.cend(), std::back_inserter(breakdownChances), [](Breakdown* breakdown) {
        return breakdown->chance();
    });

    auto distribution = std::discrete_distribution<int>(breakdownChances.begin(), breakdownChances.end());
    auto index = distribution(d->randomGenerator);

    setActiveBreakdown(d->breakdowns.at(index)->activeFromCurrent());
}

void BreakdownHandler::Private::resetTimer()
{
    auto running = timer->isActive();
    auto newInterval = timerDistribution(randomGenerator);

    if (running) {
        timer->start(newInterval);
    } else {
        timer->setInterval(newInterval);
    }
}
